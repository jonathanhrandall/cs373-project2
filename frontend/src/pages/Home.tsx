import React, { useState } from 'react';
import { Switch, Route } from 'react-router-dom';
import Image from 'react-bootstrap/Image'
import Grid from '@material-ui/core/Grid';
import { Button } from 'react-bootstrap'
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import HomeFarmerLogo from '../greenslogo.svg'
import { Form, FormControl } from 'react-bootstrap'
import GenericGrid from '../components/GenericGrid';

const useStyles = makeStyles(function () {
  return ({
    buttonPadding: {
      padding: '10px',
    }
  });
});

export default function Home() {
  const classes = useStyles();
  const [query, setQuery] = useState('');
  const [formEntry, setFormEntry] = useState('');
  function updateFormEntry({ currentTarget }: any) {
    setFormEntry(currentTarget.value);
  }
  function performQuery() {
    setQuery(formEntry);
  }

  if (!query) {
    return (
      <Grid container direction='row' justify='center' alignItems='center'
        style={{ margin: 0, width: '100%' }}>
        <Grid item>
          <Typography variant='h1' align='center'>
            Grow together.
          </Typography>
          <Typography variant='h6' align='center' gutterBottom>
            Change the way you eat.
            Learn how to farm in your own backyard.
          </Typography>
          <Switch>
            <Route>
              <Form inline >
                <FormControl style={{ width: "85%" }} size="lg" id="searchBox"
                  type="text" className="mr-sm-2"
                  placeholder={"Search for plants, cities, recipes..."}
                  onChange={updateFormEntry} onSubmit={performQuery} />
                <Button className={classes.buttonPadding} type="submit"
                  onClick={performQuery} style={{ background: '#00796b' }}>
                  Search
                </Button>{' '}
              </Form>
            </Route>
          </Switch>
        </Grid>
        <Grid item>
          <Image src={HomeFarmerLogo} alt='HomeFarmerLogo' fluid />
        </Grid>
      </Grid>
    );
  }
  return (
    <div>
      <GenericGrid modelName='Plants' initialQuery={query} />
      <GenericGrid modelName='Cities' initialQuery={query} />
      <GenericGrid modelName='Recipes' initialQuery={query} />
    </div>
  )
}