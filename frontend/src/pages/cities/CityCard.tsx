import React from 'react';
import { Link } from 'react-router-dom';
import { Card, CardActionArea, CardContent } from '@material-ui/core';
import { CardMedia, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Highlight from 'react-highlighter';

const useStyles = makeStyles(() => ({
  card: { height: "100%", display: "flex", flexDirection: "column" },
  cardMedia: { paddingTop: "56.25%" },
  cardContent: { flexGrow: 0 },
  cardActionArea: { height: "100%" }
}));

// Individual CityCard
export default function CityCard({ instance, searchTerm = "" }: any) {
  const classes = useStyles();
  const minTemp = Math.round(instance.min_temp);
  const maxTemp = Math.round(instance.max_temp);
  return (
    <Link to={`/cities/` + instance.id} style={{ textDecoration: 'none' }}>
      <CardActionArea className={classes.cardActionArea} >
        <Card className={classes.card}>
          <CardMedia className={classes.cardMedia} image={instance.image_url}
            title={instance.city_name} />
          <CardContent className={classes.cardContent}>
            <Typography gutterBottom variant="h5" component="h2">
              <Highlight search={searchTerm}>
                {instance.city_name + ", " + instance.state_code}
              </Highlight>
            </Typography>
            <Typography>State:
              <Highlight search={searchTerm}>
                {" " + instance.state_name}
              </Highlight>
            </Typography>
            <Typography>Latitude: {instance.latitude + "°"}</Typography>
            <Typography>Longitude: {instance.longitude + "°"}</Typography>
            <Typography>Humidity: {instance.humidity + "%"}</Typography>
            <Typography>
              Temp. Range: {minTemp} - {maxTemp}°F
            </Typography>
          </CardContent>
        </Card>
      </CardActionArea>
    </Link>
  );
}