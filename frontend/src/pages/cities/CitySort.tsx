import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: { margin: theme.spacing(0), width: '100%' },
    selectEmpty: { marginTop: theme.spacing(2) }
  }),
);

const sortVals = [
  { val: '', desc: 'Default' },
  { val: ['city_name', 'ascending'] + '', desc: 'City name ascending' },
  { val: ['city_name', 'descending'] + '', desc: 'City name descending' },
  { val: ['state_name', 'ascending'] + '', desc: 'State name ascending' },
  { val: ['state_name', 'descending'] + '', desc: 'State name descending' },
  { val: ['latitude', 'ascending'] + '', desc: 'Latitude ascending' },
  { val: ['latitude', 'descending'] + '', desc: 'Latitude descending' },
  { val: ['longitude', 'ascending'] + '', desc: 'Longitude ascending' },
  { val: ['longitude', 'descending'] + '', desc: 'Longitude descending' },
  { val: ['min_temp', 'ascending'] + '', desc: 'Min temp ascending' },
  { val: ['min_temp', 'descending'] + '', desc: 'Min temp descending' },
  { val: ['max_temp', 'ascending'] + '', desc: 'Max temp ascending' },
  { val: ['max_temp', 'descending'] + '', desc: 'Max temp descending' },
  { val: ['humidity', 'ascending'] + '', desc: 'Humidity ascending' },
  { val: ['humidity', 'descending'] + '', desc: 'Humidity descending' }
];

export default function CitySort({ onChange }: any) {
  const classes = useStyles();
  const [sortBy, setSortBy] = React.useState([""]);

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    const valueArray = (event.target.value as string).split(",");
    setSortBy(valueArray);
    onChange(event);
  };

  return (
    <FormControl variant="outlined" className={classes.formControl}>
      <InputLabel id="demo-simple-select-outlined-label">Sort by</InputLabel>
      <Select label="Sort by" labelId="demo-simple-select-outlined-label"
        id="demo-simple-select-outlined" value={sortBy} onChange={handleChange}>
        {sortVals.map(sortVal => (
          <MenuItem key={sortVal.val} value={sortVal.val}>
            {sortVal.desc}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
}