import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import { Box, Card, CardMedia, Container, Grid } from '@material-ui/core';
import { Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import RelatedInstances from '../../components/RelatedInstances';
import CityTable from './CityTable';
import MapSection from '../../components/Map';

const cityStyles = makeStyles((theme) => ({
  root: { flexGrow: 1 },
  paper: {
    padding: theme.spacing(2), textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  card: { maxWidth: 345 },
  cardContent: { flexGrow: 1 },
  media: { height: 0, paddingTop: '75%' },
}));

// Display content for an individual city page
export default function City() {
  const classes = cityStyles();
  const { cityId }: any = useParams();
  const [cityObject, setCityObject] = useState({
    "id": "-1", "city_name": "", "state_name": "", "state_code": "",
    "latitude": "", "longitude": "", "min_temp": "", "max_temp": "",
    "humidity": "", "pressure": "", "image_url": ""
  });

  useEffect(() => {
    axios.get('https://homefarmer.me/api/Cities/' + cityId)
      .then(response => {
        setCityObject(response.data);
      })
      .catch(error => {
        console.log(JSON.stringify(error));
      })
  }, [cityId])
  if (cityObject.id === "-1") {
    return (<h3 className="text-center">City not found.</h3>);
  } else {
    return (
      <div className={classes.root}>
        <Container maxWidth="sm">
          <Typography component="h1" variant="h2" align="center"
            color="textPrimary">
            {cityObject.city_name}
          </Typography>
          <Typography variant="h5" align="center" color="textSecondary"
            style={{ fontStyle: "oblique" }} paragraph>
            {cityObject.state_name}
          </Typography>
        </Container>
        <Container maxWidth="md">
          <Grid container spacing={4}>
            <Grid item xs >
              <Card style={{ textAlign: "center" }}>
                <CardMedia className={classes.media}
                  image={cityObject.image_url} title={cityObject.city_name} />
              </Card>
            </Grid>
            <Grid item xs >
              <Card style={{ textAlign: "center" }}>
                <MapSection location={{
                  lat: Number(cityObject.latitude),
                  lng: Number(cityObject.longitude)
                }} zoomLevel={10} mapTypeId="hybrid" />
              </Card>
            </Grid>
          </Grid>
          <Grid container spacing={4} >
            <Grid item xs >
              <Card style={{ textAlign: "center" }}>
                <CityTable key={cityObject.city_name} city={cityObject} />
              </Card>
            </Grid>
          </Grid>
          <Box m={2} >
            <Typography variant="h4" align="center" color="textPrimary" >
              Related
            </Typography>
          </Box>
          <RelatedInstances id={cityObject.id} modelName="Cities" />
        </Container>
      </div>
    );
  }
}