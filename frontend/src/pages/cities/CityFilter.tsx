import React from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: { width: '100%', '& > * + *': { marginTop: theme.spacing(3) } }
  }),
);

const states = [
  'AL', 'AK', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'FL', 'GA',
  'HI', 'ID', 'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME', 'MD',
  'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 'NE', 'NV', 'NH', 'NJ',
  'NM', 'NY', 'NC', 'ND', 'OH', 'OK', 'OR', 'PA', 'RI', 'SC',
  'SD', 'TN', 'TX', 'UT', 'VT', 'VA', 'WA', 'WV', 'WI', 'WY'
];

export default function CityFilter({ value, onChange }: any) {
  const classes = useStyles();
  const [filterBy, setFilterBy] = React.useState([] as any[]);

  function handleInputChange(event: any, value: any) {
    setFilterBy(value);
    onChange(event, value);
  }

  return (
    <div className={classes.root}>
      <Autocomplete
        multiple
        id="tags-outlined"
        options={states}
        getOptionLabel={(option) => option}
        filterSelectedOptions
        value={filterBy}
        onChange={handleInputChange}
        renderInput={(params) => (
          <TextField
            {...params}
            variant="outlined"
            label="Filter by states"
          />
        )}
      />
    </div>
  );
}