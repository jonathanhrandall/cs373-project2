import React from 'react';
import { Paper, Table, TableBody, TableCell } from '@material-ui/core';
import { TableContainer, TableRow } from '@material-ui/core';

export default function CityTable({ city }: any) {
  const tempRange = Math.round(city.min_temp) + " - " +
    Math.round(city.max_temp) + "°F";
  return (
    <TableContainer component={Paper}>
      <Table aria-label="simple table">
        <colgroup>
          <col width="50%" />
          <col width="50%" />
        </colgroup>
        <TableBody>
          <TableRow>
            <TableCell align="center">State</TableCell>
            <TableCell align="center">{city.state_name}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell align="center">Temperature Range</TableCell>
            <TableCell align="center">{tempRange}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell align="center">Humidity</TableCell>
            <TableCell align="center">{city.humidity}%</TableCell>
          </TableRow>
          <TableRow>
            <TableCell align="center">Pressure</TableCell>
            <TableCell align="center">{city.pressure / 1000} bars</TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </TableContainer>
  );
}