import React from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: { width: '100%', '& > * + *': { marginTop: theme.spacing(3) } }
  }),
);

const families = ["Actinidiaceae", "Amaranthaceae", "Amaryllidaceae",
  "Apiaceae", "Asparagaceae", "Asteraceae", "Brassicaceae", "Bromeliaceae",
  "Convolvulaceae", "Costaceae", "Cucurbitaceae", "Dioscoreaceae",
  "Ericaceae", "Euphorbiaceae", "Fabaceae", "Lamiaceae", "Poaceae",
  "Polygonaceae", "Rosaceae", "Rutaceae", "Solanaceae", "Zingiberaceae"
];

export default function RecipeFilter({ value, onChange }: any) {
  const classes = useStyles();
  const [filterBy, setFilterBy] = React.useState([] as any[]);

  function handleInputChange(event: any, value: any) {
    setFilterBy(value);
    onChange(event, value);
  }

  return (
    <div className={classes.root}>
      <Autocomplete
        multiple
        id="tags-outlined"
        options={families}
        getOptionLabel={(option) => option}
        filterSelectedOptions
        value={filterBy}
        onChange={handleInputChange}
        renderInput={(params) => (
          <TextField
            {...params}
            variant="outlined"
            label="Filter by family"
          />
        )}
      />
    </div>
  );
}
