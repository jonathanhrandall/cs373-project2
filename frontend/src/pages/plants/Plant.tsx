import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import { Box, Card, CardContent, CardActionArea } from '@material-ui/core';
import { CardMedia, Container, Grid, Typography } from '@material-ui/core';
import { makeStyles } from "@material-ui/core/styles";
import RelatedInstances from '../../components/RelatedInstances';
import PlantTable from './PlantTable';

const plantStyles = makeStyles((theme) => ({
  root: { flexGrow: 1 },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary
  },
  card: { maxWidth: 345 },
  media: { height: 0, paddingTop: '69.75%' }
}));

// Display content for an individual species page
export default function Plant() {
  const classes = plantStyles();
  let { plantId }: any = useParams();
  const [plant, setPlant] = useState({
    "common_name": "-1", "genus": "", "id": "", "family": "",
    "scientific_name": "", "image_url": "", "wiki_url": "", "vegetable": ""
  });

  useEffect(() => {
    axios.get('https://homefarmer.me/api/Plants/' + plantId)
      .then(response => {
        setPlant(response.data);
      })
      .catch(error => {
        console.log(JSON.stringify(error));
      })
  }, [plantId]);
  if (plant.common_name === "-1") {
    return (<h3 className="text-center">Plant not found.</h3>);
  } else {
    return (
      <div className={classes.root}>
        <Container maxWidth="sm">
          <Typography component="h1" variant="h2" align="center"
            color="textPrimary">
            {plant.common_name}
          </Typography>
          <Typography variant="h5" align="center" color="textSecondary"
            style={{ fontStyle: "oblique" }} paragraph>
            {plant.scientific_name}
          </Typography>
        </Container>
        <Container maxWidth="md">
          <Grid container spacing={4}>
            <Grid item xs >
              <Card style={{ textAlign: "center" }}>
                <CardActionArea href={plant.image_url}>
                  <CardMedia className={classes.media}
                    image={plant.image_url}
                    title={plant.common_name} />
                </CardActionArea>
                <CardContent>
                  <a id="link" href={plant.image_url}>Image</a>
                </CardContent>
              </Card>
            </Grid>
            <Grid item xs >
              <Card style={{ textAlign: "center" }}>
                <CardActionArea href={plant.wiki_url}>
                  <CardMedia className={classes.media}
                    image="https://i.imgur.com/NdzteMG.png"
                    title="Wikipedia" />
                </CardActionArea>
                <CardContent>
                  <a id="link" href={plant.wiki_url}>Wikipedia</a>
                </CardContent>
              </Card>
            </Grid>
          </Grid>
          <Grid container spacing={4}>
            <Grid item xs >
              <Card style={{ textAlign: "center" }}>
                <PlantTable key={plant.common_name} plant={plant} />
              </Card>
            </Grid>
          </Grid>
          <Box m={2} >
            <Typography variant="h4" align="center" color="textPrimary" >
              Related
            </Typography>
          </Box>
          <RelatedInstances id={plant.id} modelName="Plants" />
        </Container>
      </div>
    );
  }
}
