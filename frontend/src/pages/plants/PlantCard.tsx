import React from 'react';
import { Link } from 'react-router-dom';
import { Card, CardActionArea, CardContent } from '@material-ui/core';
import { CardMedia, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Highlight from 'react-highlighter';

const useStyles = makeStyles(() => ({
  card: { height: "100%", display: "flex", flexDirection: "column" },
  cardMedia: { paddingTop: "56.25%" },
  cardContent: { flexGrow: 0 },
  cardActionArea: { height: "100%" }
}));

// Individual PlantCard
export default function PlantCard({ instance, searchTerm = "" }: any) {
  const classes = useStyles();
  return (
    <Link to={`/plants/` + instance.id} style={{ textDecoration: 'none' }}>
      <CardActionArea className={classes.cardActionArea} >
        <Card className={classes.card}>
          <CardMedia className={classes.cardMedia} image={instance.image_url}
            title={instance.name} />
          <CardContent className={classes.cardContent}>
            <Typography gutterBottom variant="h5" component="h2">
              <Highlight search={searchTerm}>
                {instance.common_name}
              </Highlight>
            </Typography>
            <Typography display="inline">Genus: </Typography>
            <Typography display="inline" style={{ fontStyle: "italic" }}>
              <Highlight search={searchTerm}>
                {instance.genus}
              </Highlight>
            </Typography>
            <Typography></Typography>
            <Typography display="inline">Scientific Name: </Typography>
            <Typography display="inline" style={{ fontStyle: "italic" }}>
              <Highlight search={searchTerm}>
                {instance.scientific_name}
              </Highlight>
            </Typography>
            <Typography></Typography>
            <Typography display="inline">Family: </Typography>
            <Typography display="inline" style={{ fontStyle: "italic" }}>
              <Highlight search={searchTerm}>
                {instance.family}
              </Highlight>
            </Typography>
          </CardContent>
        </Card>
      </CardActionArea>
    </Link>
  );
}