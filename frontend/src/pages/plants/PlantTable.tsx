import React from 'react';
import { Paper, Table, TableBody, TableCell } from '@material-ui/core';
import { TableContainer, TableRow } from '@material-ui/core';

function isVegetable(isVegetable: boolean) {
  return (isVegetable)? "Yes" : "No" ;
}

export default function PlantTable({ plant }: any) {
  return (
    <TableContainer component={Paper}>
      <Table aria-label="simple table">
        <TableBody>
          <TableRow>
            <TableCell align="center">Family</TableCell>
            <TableCell align="center">{plant.family}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell align="center">Genus</TableCell>
            <TableCell align="center">{plant.genus}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell align="center">Scientific Name</TableCell>
            <TableCell align="center">{plant.scientific_name}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell align="center">Is Vegetable?</TableCell>
            <TableCell align="center">{isVegetable(plant.vegetable)}</TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </TableContainer>
  );
}