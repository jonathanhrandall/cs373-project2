import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import { Box, Card, CardContent, CardActionArea } from '@material-ui/core';
import { CardMedia, Container, Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import RelatedInstances from '../../components/RelatedInstances';
import IngredientListTable from './IngredientListTable';
import RecipeTable from './RecipeTable';

const recipeStyles = makeStyles((theme) => ({
  root: { flexGrow: 1 },
  paper: {
    padding: theme.spacing(2), textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  card: { maxWidth: 345 },
  media: { height: 0, paddingTop: '67.75%' }
}));

// Display content for an individual recipe page
export default function Recipe() {
  const classes = recipeStyles();
  const { recipeId }: any = useParams();
  const [recipeObject, setRecipeObject] = useState({
    "id": "-1", "label": "", "yield": "", "ingredient_lines": "",
    "calories": "", "total_time": "", "source": "", "url": "", "image": ""
  });

  useEffect(() => {
    axios.get('https://homefarmer.me/api/Recipes/' + recipeId)
      .then(response => {
        setRecipeObject(response.data);
      })
      .catch(error => {
        console.log(JSON.stringify(error));
      })
  }, [recipeId]);
  if (recipeObject.id === "-1") {
    return (<h3 className="text-center">Recipe not found.</h3>);
  } else {
    return (
      <div className={classes.root}>
        <Container maxWidth="sm">
          <Typography component="h1" variant="h4" align="center"
            color="textPrimary">
            {recipeObject.label}
          </Typography>
          <Typography variant="h5" align="center" color="textSecondary"
            style={{ fontStyle: "oblique" }} paragraph>
            {recipeObject.source}
          </Typography>
        </Container>
        <Container maxWidth="md">
          <Grid container spacing={3}>
            <Grid item xs >
              <Card style={{ textAlign: "center" }}>
                <CardActionArea href={recipeObject.image}>
                  <CardMedia className={classes.media}
                    image={recipeObject.image} />
                </CardActionArea>
                <CardContent>
                  <a id="link" href={recipeObject.image}>Image</a>
                </CardContent>
              </Card>
            </Grid>
            <Grid item xs >
              <Card style={{ textAlign: "center" }}>
                <CardActionArea href={recipeObject.url}>
                  <CardMedia className={classes.media}
                    image="https://i.imgur.com/ZC2guVP.jpg" />
                </CardActionArea>

                <CardContent>
                  <a href={recipeObject.url}>Source</a>
                </CardContent>
              </Card>
            </Grid>
          </Grid>
        </Container>

        <Container maxWidth="md">
          <Grid container spacing={3}>
            <Grid item xs={12} sm={6}>
              <Typography variant="h5" align="center" gutterBottom>
                Ingredients
              </Typography>
              <IngredientListTable key={recipeObject.label}
                recipe={recipeObject} />
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography variant="h5" align="center" gutterBottom>
                Info
              </Typography>
              <RecipeTable key={recipeObject.label} recipe={recipeObject} />
            </Grid>
          </Grid>
          <Box m={2} >
            <Typography variant="h4" align="center" color="textPrimary" >
              Related
            </Typography>
          </Box>
          <RelatedInstances id={recipeObject.id} modelName='Recipes' />
        </Container>
      </div>
    );
  }
}
