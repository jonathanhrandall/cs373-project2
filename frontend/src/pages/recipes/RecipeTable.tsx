import React from 'react';
import { Paper, Table, TableBody, TableCell } from '@material-ui/core';
import { TableContainer, TableRow } from '@material-ui/core';

export default function RecipeTable({ recipe }: any) {
  const total_time_label = (recipe.total_time === 0) ?
    "Instant" : recipe.total_time + " minutes";
  return (
    <TableContainer component={Paper}>
      <Table aria-label="recipe table">
        <TableBody>
          <TableRow>
            <TableCell align="center">Time</TableCell>
            <TableCell align="center">{total_time_label}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell align="center">Yield</TableCell>
            <TableCell align="center">{recipe.yield} servings</TableCell>
          </TableRow>
          <TableRow>
            <TableCell align="center">Calories</TableCell>
            <TableCell align="center">{Math.floor(recipe.calories)}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell align="center">Source</TableCell>
            <TableCell align="center">
              <a href={recipe.url}><u>{recipe.source}</u></a>
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </TableContainer>
  );
}