import React from 'react';
import { Link } from 'react-router-dom';
import { Card, CardActionArea, CardActions } from '@material-ui/core';
import { CardContent, CardMedia, Grid } from '@material-ui/core';
import { Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import CaloriesIcon from '@material-ui/icons/Fastfood';
import CookTimeIcon from '@material-ui/icons/AccessTime';
import ServingsIcon from '@material-ui/icons/People';
import Highlight from 'react-highlighter';

const useStyles = makeStyles(() => ({
  card: { height: '100%', display: "flex", flexDirection: "column" },
  cardMedia: { paddingTop: "56.25%" },
  cardContent: { flexGrow: 1 },
  cardActionArea: { justifyContent: "space-between", height: "100%" }
}));

// Individual RecipeCard
export default function RecipeCard({ instance, searchTerm = "" }: any) {
  const classes = useStyles();
  const total_time_label = (instance.total_time === 0) ?
    'Instant' : instance.total_time;
  return (
    <Link to={'/recipes/' + instance.id} style={{ textDecoration: 'none' }}>
      <CardActionArea className={classes.cardActionArea} >
        <Card className={classes.card}>
          <CardMedia className={classes.cardMedia} image={instance.image}
            title={instance.label} />
          <CardContent className={classes.cardContent}>
            <Typography gutterBottom variant="h6" component="h2">
              <Highlight search={searchTerm}>{instance.label}</Highlight>
            </Typography>
            <Typography gutterBottom >
              <Highlight search={searchTerm}>{instance.source}</Highlight>
            </Typography>
          </CardContent>
          <CardActions>
            <RecipeCardBase time={total_time_label} servings={instance.yield}
              calories={Math.floor(instance.calories)} />
          </CardActions>
        </Card>
      </CardActionArea>
    </Link>
  );
}

function RecipeCardBase(props: any) {
  const { time, servings, calories } = props;
  return (
    <Grid container spacing={2} alignItems={"baseline"}>
      <Grid item xs={4} >
        <Grid container spacing={0} direction="column"
          alignItems="center" justify="center">
          <Grid item xs>
            <Typography align="center">Time</Typography>
          </Grid>
          <Grid item xs >
            <CookTimeIcon />
          </Grid>
          <Grid item xs>
            <Typography>{time}</Typography>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={4} >
        <Grid container spacing={0} direction="column"
          alignItems="center" justify="center">
          <Grid item xs>
            <Typography align="center">Servings</Typography>
          </Grid>
          <Grid item xs >
            <ServingsIcon />
          </Grid>
          <Grid item xs >
            <Typography>{servings}</Typography>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={4} >
        <Grid container spacing={0} direction="column"
          alignItems="center" justify="center">
          <Grid item xs>
            <Typography align="center">Calories</Typography>
          </Grid>
          <Grid item xs >
            <CaloriesIcon />
          </Grid>
          <Grid item xs >
            <Typography>{calories}</Typography>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}