import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: { margin: theme.spacing(0), width: '100%' },
    selectEmpty: { marginTop: theme.spacing(2) }
  }),
);

const sortVals = [
  { val: '', desc: 'Default' },
  { val: ['label', 'ascending'] + '', desc: 'Recipe name ascending' },
  { val: ['label', 'descending'] + '', desc: 'Recipe name descending' },
  { val: ['total_time', 'ascending'] + '', desc: 'Total time ascending' },
  { val: ['total_time', 'descending'] + '', desc: 'Total time descending' },
  { val: ['yield', 'ascending'] + '', desc: 'Servings ascending' },
  { val: ['yield', 'descending'] + '', desc: 'Servings descending' },
  { val: ['calories', 'ascending'] + '', desc: 'Calories ascending' },
  { val: ['calories', 'descending'] + '', desc: 'Calories descending' }
];

export default function RecipeSort({ onChange }: any) {
  const classes = useStyles();
  const [sortBy, setSortBy] = React.useState([""]);

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    const valueArray = (event.target.value as string).split(",");
    setSortBy(valueArray);
    onChange(event);
  };

  return (
    <FormControl variant="outlined" className={classes.formControl}>
      <InputLabel id="demo-simple-select-outlined-label">Sort by</InputLabel>
      <Select label="Sort by" labelId="demo-simple-select-outlined-label"
        id="demo-simple-select-outlined" value={sortBy} onChange={handleChange}>
        {sortVals.map(sortVal => (
          <MenuItem key={sortVal.val} value={sortVal.val}>
            {sortVal.desc}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
}