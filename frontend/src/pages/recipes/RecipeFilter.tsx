import React from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: { width: '100%', '& > * + *': { marginTop: theme.spacing(3) } }
  }),
);

const sources = [
  "101 Cookbooks", "Allrecipes", "BBC", "BBC Good Food", "Chowhound", "Cookstr",
  "cravingsomethinghealthy.com", "David Lebovitz", "Donna Hay",
  "Elizabeth Minchilli in Rome", "Epicurious", "everywherefare.com",
  "Food & Wine", "Food Republic", "Food52", "homegrownandhealthy.com",
  "Honest Cooking", "James Beard Foundation", "Jamie Oliver", "justapinch.com",
  "La Tartine Gourmande", "Martha Stewart", "NY Times", "Red Cook", "Ruhlman",
  "San Francisco Gate", "Saveur", "Serious Eats", "SippitySup",
  "Smitten Kitchen", "The Daily Meal", "White On Rice Couple"
];

export default function RecipeFilter({ value, onChange }: any) {
  const classes = useStyles();
  const [filterBy, setFilterBy] = React.useState([] as any[]);

  function handleInputChange(event: any, value: any) {
    setFilterBy(value);
    onChange(event, value);
  }

  return (
    <div className={classes.root}>
      <Autocomplete multiple id="tags-outlined" options={sources}
        getOptionLabel={(option) => option} filterSelectedOptions
        value={filterBy} onChange={handleInputChange}
        renderInput={(params) => (
          <TextField
            {...params}
            variant="outlined"
            label="Filter by sources"
          />
        )}
      />
    </div>
  );
}