import React from 'react';
import { Paper, Table, TableBody, TableCell } from '@material-ui/core';
import { TableContainer, TableRow } from '@material-ui/core';

export default function IngredientListTable({ recipe }: any) {
  return (
    <TableContainer component={Paper}>
      <Table aria-label="ingredient list table">
        <TableBody>
          {Object.values(recipe.ingredient_lines).map((value: any) => (
            <TableRow key={value}>
              <TableCell align="center">{value}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}