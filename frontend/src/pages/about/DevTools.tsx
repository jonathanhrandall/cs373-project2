import React from 'react';
import { Card, CardActionArea, CardContent } from '@material-ui/core';
import { CardMedia, Container, Grid, Typography } from '@material-ui/core';
import getDevTools from './data/DevToolsData';

export default function ApisUtilized(props: any) {
  const { classes } = props;
  const devTools = getDevTools();
  return (
    <Container className={classes.cardGrid} maxWidth="md">
      <Typography variant="h3" align="center" color="textPrimary" gutterBottom>
        Development Tools
      </Typography>
      <Grid container spacing={4}>
        {devTools.map((devTool: any) => (
          <Grid item key={devTool.source} xs={12} sm={6} md={4}>
            <Card className={classes.card} style={{ textAlign: "center" }}>
              <CardActionArea href={devTool.source}>
                <CardMedia className={classes.cardMedia} image={devTool.img_url}
                  title={"Link to " + devTool.name} />
              </CardActionArea>
              <CardContent className={classes.cardContent}>
                <Typography gutterBottom variant="h5" component="h2">
                  {devTool.name}
                </Typography>
                <Typography gutterBottom component="h2">
                  {devTool.description}
                </Typography>
              </CardContent>
            </Card>
          </Grid>
        ))}
      </Grid>
    </Container>
  );
}
