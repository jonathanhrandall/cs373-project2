import React from 'react';
import { Card, CardActions, CardContent, Paper } from '@material-ui/core';
import { CardMedia, Container, Grid, Typography } from '@material-ui/core';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import ListAltIcon from '@material-ui/icons/ListAlt';
import ShareIcon from '@material-ui/icons/Share';

type TeamMember = {
  heading: string,
  img: string,
  bio: string,
  resp: string,
  num_commits: number,
  num_issues: number,
  unit_tests: number
}

export default function TeamMembers(props: any) {
  const classes = props.classes;
  const teamMembers: TeamMember[] = props.teamMembers;
  return (
    <Container className={classes.cardGrid} maxWidth="md">
      <Typography variant="h3" align="center" color="textPrimary" gutterBottom>
        Our Team
      </Typography>
      <Grid container spacing={4}>
        {teamMembers.map((card: any) => (
          <Grid item key={card.heading} xs={12} sm={6} md={4}>
            <Card className={classes.card}>
              <CardMedia className={classes.cardMedia} image={card.img}
                title={card.heading} />
              <CardContent className={classes.cardContent}>
                <Typography gutterBottom variant="h5" component="h2">
                  {card.heading}
                </Typography>
                <Typography gutterBottom variant="h6" component="h2">
                  {card.resp}
                </Typography>
                <Typography>
                  {card.bio}
                </Typography>
              </CardContent>
              <CardActions disableSpacing>
                <div className={classes.root}>
                  <Grid container spacing={0}>
                    <Grid item xs={4}>
                      <Paper className={classes.paper} elevation={0}>
                        <Typography gutterBottom component="h2">
                          Commits
                        </Typography>
                        <Grid container>
                          <Grid item xs={12} sm={6}>
                            <ShareIcon />
                          </Grid>
                          <Grid item xs={12} sm={6}>
                            <Typography>{card.num_commits}</Typography>
                          </Grid>
                        </Grid>
                      </Paper>
                    </Grid>
                    <Grid item xs={4}>
                      <Paper className={classes.paper} elevation={0}>
                        <Typography gutterBottom component="h2">
                          Issues
                        </Typography>
                        <Grid container>
                          <Grid item xs={12} sm={6}>
                            <ListAltIcon />
                          </Grid>
                          <Grid item xs={12} sm={6}>
                            <Typography>{card.num_issues}</Typography>
                          </Grid>
                        </Grid>
                      </Paper>
                    </Grid>
                    <Grid item xs={4}>
                      <Paper className={classes.paper} elevation={0}>
                        <Typography gutterBottom component="h2">
                          Tests
                        </Typography>
                        <Grid container>
                          <Grid item xs={12} sm={6}>
                            <CheckCircleOutlineIcon />
                          </Grid>
                          <Grid item xs={12} sm={6}>
                            <Typography>{card.unit_tests}</Typography>
                          </Grid>
                        </Grid>
                      </Paper>
                    </Grid>
                  </Grid>
                </div>
              </CardActions>
            </Card>
          </Grid>
        ))}
      </Grid>
    </Container>
  );
}
