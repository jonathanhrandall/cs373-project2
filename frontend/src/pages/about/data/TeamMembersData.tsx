export default function getTeamMembers() {
  return [
    {
      heading: "Jonathan Randall",
      img: "https://i.imgur.com/EnIv0gi.png",
      bio: "I'm a third year CS and Plan II major at UT Austin. I'm from Dall" +
        "as, Texas, and I love producing music, cooking and looking at pictur" +
        "es of buildings online.",
      resp: "Front-end",
      num_commits: 0,
      num_issues: 0,
      unit_tests: 0
    },
    {
      heading: "Grant He",
      img: "https://i.imgur.com/FgthWRz.jpg",
      bio: "I'm a third year CS major at UT Austin. I was born and raised in " +
        "the Austin area and haven't stopped repping the Longhorns since. In " +
        "my free time I enjoy cooking, watching screwball comedies, and backp" +
        "acking the great outdoors.",
      resp: "Front-end",
      num_commits: 0,
      num_issues: 0,
      unit_tests: 0
    },
    {
      heading: "Sujoy Purkayastha",
      img: "https://i.imgur.com/SF7SSc1.png",
      bio: "I'm a third year CS major at UT Austin. I'm from Plano, Texas and" +
        " I enjoy fishkeeping and exploring downtown Austin.",
      resp: "Back-end",
      num_commits: 0,
      num_issues: 0,
      unit_tests: 0
    },
    {
      heading: "Pranav Akinepalli",
      img: "https://i.imgur.com/Nh8jf6E.jpg",
      bio: "I'm a third year CS major and Business minor at UT Austin. I'm fr" +
        "om Irving, Texas and have lived there for most of my life. My hobbie" +
        "s include playing video games, spending time outside in nature, and " +
        "traveling (so I can try lots of different foods)!",
      resp: "Back-end (Team Lead)",
      num_commits: 0,
      num_issues: 0,
      unit_tests: 0
    },
    {
      heading: "Sameer Haniyur",
      img: "https://i.imgur.com/xsNpZQc.jpg",
      bio: "I'm a third year CS major living in Flower Mound, Texas. I'm orig" +
        "inally from Florida just outside Miami, but I moved to Texas when I " +
        "was one. My favorite hobby outside of some light gaming is playing g" +
        "olf on the hundreds of courses in the Dallas area.",
      resp: "Back-end",
      num_commits: 0,
      num_issues: 0,
      unit_tests: 0
    }
  ];
} 