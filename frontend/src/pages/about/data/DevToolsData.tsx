export default function getDevTools() {
  return [
    {
      name: "React",
      source: "https://reactjs.org/",
      img_url: "https://i.imgur.com/mRZA6lX.png",
      description: "JavaScript library for frontend development"
    },
    {
      name: "React Bootstrap",
      source: "https://react-bootstrap.github.io/",
      img_url: "https://i.imgur.com/EbXt6Iv.png",
      description: "React components library"
    },
    {
      name: "Material UI",
      source: "https://material-ui.com/",
      img_url: "https://i.imgur.com/0AyGOAM.png",
      description: "React components library"
    },
    {
      name: "TypeScript",
      source: "https://www.typescriptlang.org/",
      img_url: "https://i.imgur.com/MVxWCSI.png",
      description: "JavaScript for application-scale development"
    },
    {
      name: "Postman",
      source: "https://postman.com/",
      img_url: "https://i.imgur.com/67OZVHH.png",
      description: "Tool to design and test APIs"
    },
    {
      name: "Jest",
      source: "https://jestjs.io/",
      img_url: "https://i.imgur.com/cBgtvCx.png",
      description: "JavaScript testing framework"
    },
    {
      name: "Selenium",
      source: "https://www.selenium.dev/",
      img_url: "https://i.imgur.com/18SVXuM.png",
      description: "Browser automation framework for UI testing"
    },
    {
      name: "Flask",
      source: "https://flask.palletsprojects.com/",
      img_url: "https://i.imgur.com/A1zPQql.png",
      description: "API development framework"
    },
    {
      name: "PostgreSQL",
      source: "https://www.postgresql.org/",
      img_url: "https://i.imgur.com/eTbLzPl.png",
      description: "Relational database management system"
    },
    {
      name: "GitLab",
      source: "https://gitlab.com/",
      img_url: "https://i.imgur.com/dLQSqdI.png",
      description: "Git repository and CI/CD platform"
    },
    {
      name: "Kubernetes",
      source: "https://kubernetes.io/",
      img_url: "https://i.imgur.com/qjd5SoE.png",
      description: "Container-orchestration system for CI"
    },
    {
      name: "Google Cloud Platform",
      source: "https://cloud.google.com/",
      img_url: "https://i.imgur.com/qKcPKQs.png",
      description: "Cloud hosting platform for CI"
    },
    {
      name: "Amazon Web Services",
      source: "https://aws.amazon.com/",
      img_url: "https://i.imgur.com/9IyXoJN.png",
      description: "Cloud hosting platform for CD"
    },
    {
      name: "Docker",
      source: "https://www.docker.com/",
      img_url: "https://i.imgur.com/ujwwJVb.png",
      description: "Containerization tool for consistent runtime environments"
    },
    {
      name: "Slack",
      source: "https://slack.com/",
      img_url: "https://i.imgur.com/qteIppl.jpg",
      description: "Team communication platform"
    }
  ];
}