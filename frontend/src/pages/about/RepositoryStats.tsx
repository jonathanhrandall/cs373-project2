import React from 'react';
import { Card, CardContent } from '@material-ui/core';
import { Container, Grid, Typography } from '@material-ui/core';

export default function RepositoryStats(props: any) {
  const { numCommits, numIssues, numTests } = props.stats;
  const classes = props.classes;
  return (
    <Container className={classes.cardGrid} maxWidth="md">
      <Typography variant="h3" align="center" color="textPrimary"
        gutterBottom>Repository Statistics</Typography>
      <Grid container spacing={4}>
        <Grid item xs={12} sm={6} md={4}>
          <Card className={classes.card} style={{ textAlign: "center" }}>
            <CardContent className={classes.cardContent}>
              <Typography gutterBottom variant="h5" component="h2">
                Total Commits
              </Typography>
              <Typography gutterBottom variant="h6" component="h2">
                {numCommits}
              </Typography>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <Card className={classes.card} style={{ textAlign: "center" }}>
            <CardContent className={classes.cardContent}>
              <Typography gutterBottom variant="h5" component="h2">
                Total Issues
              </Typography>
              <Typography gutterBottom variant="h6" component="h2">
                {numIssues}
              </Typography>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <Card className={classes.card} style={{ textAlign: "center" }}>
            <CardContent className={classes.cardContent}>
              <Typography gutterBottom variant="h5" component="h2">
                Total Tests
              </Typography>
              <Typography gutterBottom variant="h6" component="h2">
                {numTests}
              </Typography>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Container>
  );
}