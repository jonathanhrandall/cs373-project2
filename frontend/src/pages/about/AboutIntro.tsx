import React from 'react';
import { Container, Typography } from '@material-ui/core';

export default function AboutIntro(classes: any) {
  return (
    <div className={classes.heroContent}>
      <Container maxWidth="sm">
        <Typography component="h1" variant="h2" align="center"
          color="textPrimary" gutterBottom>
          About
        </Typography>
        <Typography variant="h5" align="center" color="textSecondary">
          <ul>
            <li>How do we feed our urbanizing world?</li>
            <li>How do we make our food systems resilient?</li>
            <li>Our goal is to promote urban agriculture and enhance
            urban food security.</li>
          </ul>
        </Typography>
      </Container>
    </div>
  );
}
