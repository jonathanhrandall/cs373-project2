// TODO: Find way to automate.
const testCounts = {
  "testers": [
    {
      "name": "Grant He",
      "num_tests": 35
    },
    {
      "name": "Jonathan Randall",
      "num_tests": 4
    },
    {
      "name": "Pranav Akinepalli",
      "num_tests": 60
    },
    {
      "name": "Sameer Haniyur",
      "num_tests": 6
    },
    {
      "name": "Sujoy Purkayastha",
      "num_tests": 3
    }
  ]
};

type TeamMember = {
  heading: string,
  img: string,
  bio: string,
  resp: string,
  num_commits: number,
  num_issues: number,
  unit_tests: number
}

export default async function getRepositoryStats(teamMembers: TeamMember[]) {
  const repo_api = "https://gitlab.com/api/v4/projects/21176938/";
  let numCommits = 0, numIssues = 0, numTests = 0;

  // Handle Commits
  let commit_response = await fetch(repo_api + "repository/contributors");
  let contributors = await commit_response.json();
  contributors.forEach(function (contributor: any) {
    let { name, commits } = contributor
    if (name === "jonathanhrandall") {
      name = "Jonathan Randall"
    }
    teamMembers.forEach(function (teamMember) {
      if (teamMember.heading === name) {
        teamMember.num_commits += commits
      }
    })
    numCommits += commits;
  });

  // Handle Issues
  let issue_response = await fetch(repo_api + "issues");
  let issues = await issue_response.json();
  issues.forEach(function (issue: any) {
    const { assignees } = issue
    if (assignees.length !== 0) {
      assignees.forEach(function (assignee: any) {
        let { name } = assignee
        if (name === "Jonathan H Randall") {
          name = "Jonathan Randall"
        }
        teamMembers.forEach(function (teamMember) {
          if (teamMember.heading === name) {
            teamMember.num_issues += 1
          }
        })
      })
    }
    numIssues += 1;
  });

  // Handle Tests
  const { testers } = testCounts;
  testers.forEach(function (tester: any) {
    const { name, num_tests } = tester
    teamMembers.forEach(function (teamMember) {
      if (teamMember.heading === name) {
        teamMember.unit_tests += num_tests;
        numTests += num_tests;
      }
    });
  });

  return {
    "numCommits": numCommits,
    "numIssues": numIssues,
    "numTests": numTests,
    "teamMembers": teamMembers
  }
}