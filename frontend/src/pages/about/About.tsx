import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import getRepositoryStats from './GetRepositoryStats';
import AboutIntro from './AboutIntro';
import ApisUtilized from './ApisUtilized';
import DevTools from './DevTools';
import RepositoryStats from './RepositoryStats';
import TeamMembers from './TeamMembers';
import UsefulLinks from './UsefulLinks';
import getTeamMembers from './data/TeamMembersData';
import LoadingSpinner from '../../components/LoadingSpinner';

const useStyles = makeStyles(theme => ({
  icon: { marginRight: theme.spacing(2) },
  cardGrid: { paddingTop: theme.spacing(8), paddingBottom: theme.spacing(8) },
  card: { height: "100%", display: "flex", flexDirection: "column" },
  cardMedia: { paddingTop: "100%" },
  cardContent: { flexGrow: 1 },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6)
  },
  root: { flexGrow: 1 },
  paper: {
    padding: theme.spacing(2), textAlign: 'center',
    color: theme.palette.text.secondary
  }
}));

export default function About() {
  const [numCommits, setNumCommits]: [number, any] = useState(0);
  const [numIssues, setNumIssues]: [number, any] = useState(0);
  const [numTests, setNumTests]: [number, any] = useState(0);
  const [teamMembers, setMembers]: [any, any] = useState([]);
  const [loading, setLoading]: [boolean, any] = useState(true);

  useEffect(() => {
    const retrieveData = async () => {
      const repositoryStats = await getRepositoryStats(getTeamMembers());
      // Set up stats for first call only
      if (teamMembers.length === 0) {
        setNumCommits(repositoryStats.numCommits);
        setNumIssues(repositoryStats.numIssues);
        setNumTests(repositoryStats.numTests);
        setMembers(repositoryStats.teamMembers);
        setLoading(false);
      }
    }
    retrieveData();
  });

  const classes = useStyles();

  if (loading) {
    return (<LoadingSpinner pageTitle="About" />);
  } else {
    return (
      <main>
        <AboutIntro classes={classes} />
        <TeamMembers classes={classes} teamMembers={teamMembers} />
        <RepositoryStats classes={classes}
          stats={{
            numCommits: numCommits,
            numIssues: numIssues,
            numTests: numTests
          }}
        />
        <ApisUtilized classes={classes} />
        <DevTools classes={classes} />
        <UsefulLinks classes={classes} />
      </main>
    );
  }
}