import React from 'react';
import { Card, CardActionArea, CardContent } from '@material-ui/core';
import { CardMedia, Container, Grid, Typography } from '@material-ui/core';

const usefulLinks = [
  {
    name: "GitLab Repository",
    source: "https://gitlab.com/jonathanhrandall/cs373-project2/",
    img_url: "https://i.imgur.com/dLQSqdI.png"
  },
  {
    name: "API Documentation",
    source: "https://documenter.getpostman.com/view/12307802/TVRdAX6w",
    img_url: "https://i.imgur.com/67OZVHH.png"
  }
];

export default function (props: any) {
  const { classes } = props;
  return (
    <Container className={classes.cardGrid} maxWidth="md">
      <Typography variant="h3" align="center" color="textPrimary" gutterBottom>
        Useful Links
      </Typography>
      <Grid container spacing={4}>
        {usefulLinks.map((usefulLink: any) => (
          <Grid item key={usefulLink.source} xs={12} sm={6} md={6}>
            <Card className={classes.card} style={{ textAlign: "center" }}>
              <CardActionArea href={usefulLink.source}>
                <CardMedia
                  className={classes.cardMedia}
                  image={usefulLink.img_url}
                  title={"Link to " + usefulLink.name} />
              </CardActionArea>
              <CardContent className={classes.cardContent}>
                <Typography gutterBottom variant="h5" component="h2">
                  {usefulLink.name}
                </Typography>
              </CardContent>
            </Card>
          </Grid>
        ))}
      </Grid>
    </Container>
  );
}