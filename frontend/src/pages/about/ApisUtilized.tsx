import React from 'react';
import { Card, CardActionArea, CardContent } from '@material-ui/core';
import { CardMedia, Container, Grid, Typography } from '@material-ui/core';

const apisUtilized = [
  {
    name: "Trefle",
    source: "https://trefle.io/",
    img_url: "https://i.imgur.com/j9cvWTK.png",
    description: "Used to find information about plant species"
  },
  {
    name: "Edamam",
    source: "https://www.edamam.com/",
    img_url: "https://i.imgur.com/RKea6xH.png",
    description: "Used to find recipe information"
  },
  {
    name: "Open Weather",
    source: "https://openweathermap.org/api",
    img_url: "https://i.imgur.com/T9FutE2.png",
    description: "Used to find regional geoclimate information"
  }
];

export default function ApisUtilized(props: any) {
  const { classes } = props;
  return (
    <Container className={classes.cardGrid} maxWidth="md">
      <Typography variant="h3" align="center" color="textPrimary" gutterBottom>
        APIs Utilized
      </Typography>
      <Grid container spacing={4}>
        {apisUtilized.map((apiUtilized: any) => (
          <Grid item key={apiUtilized.name}
            xs={12} sm={6} md={4}>
            <Card className={classes.card}
              style={{ textAlign: "center" }}>
              <CardActionArea href={apiUtilized.source}>
                <CardMedia className={classes.cardMedia}
                  image={apiUtilized.img_url}
                  title={"Link to " + apiUtilized.name} />
              </CardActionArea>
              <CardContent className={classes.cardContent}>
                <Typography gutterBottom variant="h5" component="h2">
                  {apiUtilized.name}
                </Typography>
                <Typography gutterBottom variant="h6" component="h2">
                  {apiUtilized.description}
                </Typography>
              </CardContent>
            </Card>
          </Grid>
        ))}
      </Grid>
    </Container>
  );
}
