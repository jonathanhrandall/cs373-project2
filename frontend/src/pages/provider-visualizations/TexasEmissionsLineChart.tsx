import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { CartesianGrid, Tooltip, Legend } from 'recharts';
import { LineChart, Line, XAxis, YAxis } from 'recharts';
import LoadingSpinner from '../../components/LoadingSpinner';

const token = process.env.REACT_APP_PROVIDER_API_KEY;
const query = '{"filters":[{"name":"name","op":"eq","val":"Texas"}]}';
export default function TexasEmissionsLineChart() {
  const [data_array, setDataArray] = useState([]);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    axios.get('https://api.powernet.energy/api/state?q=' + query,
      {
        headers:
          { "Authorization": `Bearer ${token}` }
      }).then(response => {
        setDataArray(response.data.objects[0].emissions);
        setLoading(false);
      })
      .catch(error => {
        console.log(JSON.stringify(error));
      });
  }, []);

  if (loading) {
    return <LoadingSpinner />;
  } else {
    return (
      <LineChart width={700} height={300} data={data_array}
        margin={{ top: 20, right: 30, left: 50, bottom: 5 }}>
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="year" />
        <YAxis yAxisId="left" unit=" MMT"
          tickFormatter={tick => { return tick / 1000000; }} />
        <YAxis yAxisId="right" orientation="right" unit=" MMT"
          tickFormatter={tick => { return tick / 1000000; }} />
        <Tooltip />
        <Legend />
        <Line yAxisId="left" type="monotone" dataKey="co2" unit=" MT"
          stroke='#0088FE' activeDot={{ r: 8 }} />
        <Line yAxisId="right" type="monotone" dataKey="nox" unit=" MT"
          stroke='#3a62ba' />
        <Line yAxisId="right" type="monotone" dataKey="so2" unit=" MT"
          stroke='#00C49F' />
      </LineChart>
    );
  }
}