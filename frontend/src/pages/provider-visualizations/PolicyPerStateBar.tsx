import React, { useState, useEffect } from 'react';
import { Cell, Tooltip, BarChart, Bar, CartesianGrid } from 'recharts';
import { XAxis, YAxis } from 'recharts';
import axios from 'axios';
import LoadingSpinner from '../../components/LoadingSpinner';
import countsFromJSON from '../../components/countsFromJSON';

const COLORS = [
  '#0088FE', '#3a62ba', '#00C49F', '#FFBB28', '#db5618', '#FF8042', '#845cbf'
];

const _dataset = [
  { "name": "CA", "policies": 360 }, { "name": "MN", "policies": 205 },
  { "name": "WA", "policies": 204 }, { "name": "CO", "policies": 194 },
  { "name": "TX", "policies": 193 }, { "name": "OR", "policies": 182 },
  { "name": "FL", "policies": 170 }, { "name": "MA", "policies": 166 },
  { "name": "NY", "policies": 157 }, { "name": "WI", "policies": 138 },
  { "name": "NC", "policies": 135 }, { "name": "MI", "policies": 135 },
  { "name": "CT", "policies": 129 }, { "name": "MD", "policies": 121 },
  { "name": "IL", "policies": 119 }, { "name": "IN", "policies": 110 },
  { "name": "AZ", "policies": 101 }, { "name": "OH", "policies": 100 },
  { "name": "IA", "policies": 100 }, { "name": "VA", "policies": 96 },
  { "name": "PA", "policies": 96 }, { "name": "MO", "policies": 92 },
  { "name": "KY", "policies": 92 }, { "name": "GA", "policies": 91 },
  { "name": "SC", "policies": 85 }, { "name": "TN", "policies": 82 },
  { "name": "NJ", "policies": 77 }, { "name": "VT", "policies": 75 },
  { "name": "NH", "policies": 67 }, { "name": "NM", "policies": 66 },
  { "name": "AL", "policies": 65 }, { "name": "ID", "policies": 63 },
  { "name": "MT", "policies": 60 }, { "name": "RI", "policies": 56 },
  { "name": "AR", "policies": 54 }, { "name": "UT", "policies": 54 },
  { "name": "US", "policies": 54 }, { "name": "DE", "policies": 51 },
  { "name": "HI", "policies": 51 }, { "name": "NV", "policies": 50 },
  { "name": "OK", "policies": 47 }, { "name": "WY", "policies": 47 },
  { "name": "ME", "policies": 43 }, { "name": "KS", "policies": 42 },
  { "name": "SD", "policies": 41 }, { "name": "LA", "policies": 40 },
  { "name": "MS", "policies": 40 }, { "name": "NE", "policies": 33 },
  { "name": "DC", "policies": 30 }, { "name": "PR", "policies": 30 },
  { "name": "AK", "policies": 28 }, { "name": "ND", "policies": 27 },
  { "name": "WV", "policies": 19 }, { "name": "VI", "policies": 14 },
  { "name": "GU", "policies": 5 }, { "name": "MP", "policies": 5 },
  { "name": "AS", "policies": 1 }, { "name": "PW", "policies": 1 },
  { "name": "MH", "policies": 1 }, { "name": "FM", "policies": 1 }]

const fetchNewData = false;
function concatenateData(response: any, setDataset: any, setLoading: any,
  totalPages: number) {
  entire_data_array = entire_data_array.concat(response.data.objects);
  loadedPage++;
  if (loadedPage >= totalPages) {
    setDataset(countsFromJSON(entire_data_array, "policies"));
    setLoading(false);
  }
}
let entire_data_array: any = [];
let loadedPage = 1;

const token = process.env.REACT_APP_PROVIDER_API_KEY;
export default function PolicyPerStatePie() {
  const [dataset, setDataset] = useState(_dataset);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    if (fetchNewData) {
      let page = 0;
      let totalPages = 896;
      do {
        page++;
        axios.get('https://api.powernet.energy/api/policy?page=' + page,
          {
            headers:
              { "Authorization": `Bearer ${token}` }
          }).then(response => {
            concatenateData(response, setDataset, setLoading, totalPages)
          })
          .catch(error => {
            console.log(JSON.stringify(error));
          });
      }
      while (page < totalPages)
    }
    else {
      setLoading(false);
    }

  }, []);

  if (loading) {
    return <LoadingSpinner />;
  } else {
    return (
      <BarChart width={900} height={300} data={dataset}
        margin={{ top: 20, right: 30, left: 20, bottom: 5 }}>
        <Tooltip />
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" interval={0} tick={{ fontSize: '7px' }} />
        <YAxis />
        <Bar dataKey="policies" fill="#8884d8" >
          {dataset.map((entry, index) => (
            <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
          ))
          }
        </Bar>
        <Tooltip />
      </BarChart>
    );
  }
}