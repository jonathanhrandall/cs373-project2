import React, { useState, useEffect } from 'react';
import { PieChart, Pie, Sector, Cell } from 'recharts';
import axios from 'axios';
import LoadingSpinner from '../../components/LoadingSpinner';
import countsFromJSON from '../../components/countsFromJSON';

const colors = [
  '#845cbf', '#3a62ba', '#0088FE', '#00C49F', '#FFBB28', '#FF8042', '#db5618'
];

const _dataset = [
  { name: "Solar", plants: 2290 },
  { name: "Gas", plants: 1741 },
  { name: "Hydro", plants: 1457 },
  { name: "Wind", plants: 1043 },
  { name: "Oil", plants: 847 },
  { name: "Waste", plants: 567 },
  { name: "Coal", plants: 338 },
  { name: "Biomass", plants: 156 },
  { name: "Geothermal", plants: 65 },
  { name: "Nuclear", plants: 61 },
  { name: "Storage", plants: 58 },
  { name: "Cogeneration", plants: 34 },
  { name: "Other", plants: 17 },
  { name: "Petcoke", plants: 12 },
]

const fetchNewData = false;
function concatenateData(response: any, setDataset: any, setLoading: any,
  totalPages: number) {
  entire_data_array = entire_data_array.concat(response.data.objects);
  loadedPage++;
  if (loadedPage >= totalPages) {
    setDataset(countsFromJSON(entire_data_array, "plants"));
    setLoading(false);
  }
}

let entire_data_array: any = [];
let loadedPage = 1;

const token = process.env.REACT_APP_PROVIDER_API_KEY;
export default function PowerBarChart() {
  const [dataset, setDataset] = useState(_dataset);
  const [loading, setLoading] = useState(true);
  const [activeIndex, setActiveIndex] = useState(0);

  useEffect(() => {
    if (fetchNewData) {
      let page = 0;
      let totalPages = 896;
      do {
        page++;
        axios.get('https://api.powernet.energy/api/powerplant?page=' + page,
          {
            headers:
              { "Authorization": `Bearer ${token}` }
          }).then(response => {
            concatenateData(response, setDataset, setLoading, totalPages)
          })
          .catch(error => {
            console.log(JSON.stringify(error));
          });
      }
      while (page < totalPages)
    }
    else {
      setLoading(false);
    }

  }, []);

  let onPieEnter = (dataset: any, index: number) => {
    setActiveIndex(index);
  };

  if (loading) {
    return <LoadingSpinner />;
  } else {
    return (
      <div>
        <PieChart width={800} height={300}>
          <Pie activeIndex={activeIndex} activeShape={renderActiveShape}
            data={dataset} cx={400} cy={250} innerRadius={0} outerRadius={200}
            startAngle={0} endAngle={180} minAngle={1}
            fill="#8884d8" dataKey="plants" onMouseEnter={onPieEnter} >
            {dataset.map((entry, index) => (
              <Cell key={`cell-${index}`}
                fill={colors[index % colors.length]} />))
            }
          </Pie>
        </PieChart>
      </div>
    );
  }
}

const renderActiveShape = (props: any) => {
  const RADIAN = Math.PI / 180;
  const { cx, cy, fill, midAngle, innerRadius, payload } = props;
  const { outerRadius, startAngle, endAngle, value } = props;
  const sin = Math.sin(-RADIAN * midAngle);
  const cos = Math.cos(-RADIAN * midAngle);
  const mx = cx + (outerRadius + 10) * cos;
  const my = cy + (outerRadius + 30) * sin;
  const ex = mx + (cos >= 0 ? 1 : -1) * 10;
  const ey = my;
  const textAnchor = cos >= 0 ? 'start' : 'end';
  return (
    <g>
      <Sector cx={cx} cy={cy} fill={fill}
        innerRadius={innerRadius} outerRadius={outerRadius}
        startAngle={startAngle} endAngle={endAngle} />
      <Sector cx={cx} cy={cy} fill={fill} innerRadius={outerRadius + 6}
        outerRadius={outerRadius + 10} startAngle={startAngle}
        endAngle={endAngle} />
      <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} textAnchor={textAnchor}
        fill="#333">
        {`${payload.name}`}
      </text>
      <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} dy={18}
        textAnchor={textAnchor} fill="#999">
        {`${value} plants`}
      </text>
    </g>
  );
};