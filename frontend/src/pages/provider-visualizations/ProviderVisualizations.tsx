import React from 'react';
import { Container, Typography } from '@material-ui/core';
import PowerplantWedgeChart from './PowerplantWedgeChart';
import TexasEmissionsLineChart from './TexasEmissionsLineChart';
import PolicyPerStateBar from './PolicyPerStateBar';

export default function ProviderVisualizations() {
  return (
    <div>
      <Container maxWidth={"xl"} >
        <Typography component="h1" variant="h2" align="center"
          color="textPrimary" gutterBottom>Provider Visualizations</Typography>
        <Typography />

        <Typography variant="h5" align="center">
          Emissions in Texas by Year in million metric tons (MMT)
        </Typography>
        <div style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          paddingBottom: '80px',
        }}>
          <TexasEmissionsLineChart />
        </div>
        <Typography variant="h5" align="center" >
          Policies per state
        </Typography>
        <div style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          paddingBottom: '80px',
        }}>
          <PolicyPerStateBar />
        </div>

        <Typography variant="h5" align="center" >
          Powerplants by fuel type
        </Typography>
        <div style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          paddingBottom: '80px',
        }}>
          <PowerplantWedgeChart />
        </div>

        <Typography variant="h6" align="center" gutterBottom>
          Data provided by <u>
            <a href="https://www.powernet.energy/">PowerNet</a></u>
        </Typography>
      </Container>
    </div>
  );
}
