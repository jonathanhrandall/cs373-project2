import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { PieChart, Pie, Sector, Cell } from 'recharts';
import LoadingSpinner from '../../components/LoadingSpinner';
import countsFromJSON from '../../components/countsFromJSON';

const renderActiveShape = (props: any) => {
  const RADIAN = Math.PI / 180;
  const { cx, cy, fill, midAngle, innerRadius, percent, payload } = props;
  const { outerRadius, startAngle, endAngle, value } = props;
  const sin = Math.sin(-RADIAN * midAngle);
  const cos = Math.cos(-RADIAN * midAngle);
  const sx = cx + (outerRadius + 10) * cos;
  const sy = cy + (outerRadius + 10) * sin;
  const mx = cx + (outerRadius + 30) * cos;
  const my = cy + (outerRadius + 30) * sin;
  const ex = mx + (cos >= 0 ? 1 : -1) * 22;
  const ey = my;
  const textAnchor = cos >= 0 ? 'start' : 'end';

  return (
    <g>
      <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>
        {payload.label}
      </text>
      <Sector cx={cx} cy={cy} fill={fill}
        innerRadius={innerRadius} outerRadius={outerRadius}
        startAngle={startAngle} endAngle={endAngle} />
      <Sector cx={cx} cy={cy} fill={fill} innerRadius={outerRadius + 6}
        outerRadius={outerRadius + 10} startAngle={startAngle}
        endAngle={endAngle} />
      <path d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`} stroke={fill}
        fill="none" />
      <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none" />
      <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} textAnchor={textAnchor}
        fill="#333">
        {`${value} recipe(s)`}
      </text>
      <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} dy={18}
        textAnchor={textAnchor} fill="#999">
        {`(${(percent * 100).toFixed(2)}%)`}
      </text>
    </g>
  );
};

export default function RecipePieChart({ colors }: any) {
  const [data_array, setDataArray] = useState([]);
  const [loading, setLoading] = useState(true);
  const [activeIndex, setActiveIndex] = useState(0);

  useEffect(() => {
    axios.get('https://homefarmer.me/api/Recipes')
      .then(response => {
        setDataArray(response.data.recipes);
        setLoading(false);
      })
      .catch(error => {
        console.log(JSON.stringify(error));
      });
  }, []);

  let onPieEnter = (data: any, index: number) => {
    setActiveIndex(index);
  };

  if (loading) {
    return <LoadingSpinner />;
  } else {
    let dataset = countsFromJSON(data_array, "source");
    return (
      <div>
        <PieChart width={800} height={550}>
          <Pie activeIndex={activeIndex}
            activeShape={renderActiveShape}
            data={dataset}
            cx={400}
            cy={275}
            startAngle={0}
            endAngle={360}
            innerRadius={150}
            outerRadius={200}
            fill="#8884d8"
            dataKey="value"
            onMouseEnter={onPieEnter} >
            {dataset.map((entry, index) => (
              <Cell key={`cell-${index}`}
                fill={colors[index % colors.length]} />))
            }
          </Pie>
        </PieChart>
      </div>
    );
  }
}
