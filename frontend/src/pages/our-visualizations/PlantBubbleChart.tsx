import React, { useState, useEffect } from 'react';
import axios from 'axios';
import BubbleChart from '@weknow/react-bubble-chart-d3';
import LoadingSpinner from '../../components/LoadingSpinner';
import countsFromJSON from '../../components/countsFromJSON';

export default function PlantBubbleChart() {
  const [data_array, setDataArray] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    axios.get('https://homefarmer.me/api/Plants')
      .then(response => {
        setDataArray(response.data.plants);
        setLoading(false);
      })
      .catch(error => {
        console.log(JSON.stringify(error));
      });
  }, []);
  if (loading) {
    return <LoadingSpinner />;
  } else {
    let dataset = countsFromJSON(data_array, "family");
    return (
      <BubbleChart graph={{ zoom: 1, offsetX: 0, offsetY: 0 }}
        width={900} height={720} fontFamily="Arial" data={dataset} />
    );
  }
}
