import React from 'react';
import { Container, Typography } from '@material-ui/core';
import RecipePieChart from './RecipePieChart';
import PlantBubbleChart from './PlantBubbleChart';
import CityScatterPlot from './CityScatterPlot';

const colors = [
  '#845cbf', '#3a62ba', '#0088FE', '#00C49F', '#FFBB28', '#FF8042', '#db5618'
];
export default function OurVisualizations() {
  return (
    <div>
      <Container maxWidth={"xl"} >
        <Typography component="h1" variant="h2" align="center"
          color="textPrimary" gutterBottom>Our Visualizations</Typography>
        <Typography />

        <Typography variant="h5" align="center" >
          Plants by Family
        </Typography>
        <div style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          paddingBottom: '80px',
        }}>
          <PlantBubbleChart />
        </div>

        <Typography variant="h5" align="center" >
          Recipe Sources
        </Typography>
        <div style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          paddingBottom: '80px',
        }}>
          <RecipePieChart colors={colors} />
        </div>

        <Typography variant="h5" align="center" >
          City Max Temperature vs. Latitude
        </Typography>
        <div style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          paddingBottom: '80px',
        }}>
          <CityScatterPlot colors={colors} />
        </div>
      </Container>
    </div>
  );
}
