import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { CartesianGrid, ScatterChart, Scatter, Cell, Tooltip } from 'recharts';
import { XAxis, YAxis, ZAxis } from 'recharts';

export default function ScatterPlot({ colors }: any) {
  const [dataset, setDataset] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    axios.get('https://homefarmer.me/api/Cities')
      .then(response => {
        setDataset(response.data.cities);
        setLoading(false);
      })
      .catch(error => {
        console.log(JSON.stringify(error));
      });
  }, []);

  if (loading) {
    return <div />;
  } else {
    return (
      <ScatterChart width={800} height={600} margin={{
        top: 50, right: 20, bottom: 20, left: 20
      }}>
        <CartesianGrid />
        <XAxis type="number" dataKey="latitude" name="Latitude" unit="°" />
        <YAxis type="number" dataKey="max_temp" name="Max temperature"
          unit="°F" />
        <ZAxis type="category" dataKey="city_name" name="City" unit="" />
        <Tooltip cursor={{ strokeDasharray: '3 3' }} />
        <Scatter name="City" data={dataset} fill="#8884d8">
          {dataset.map((entry, index) => (
            <Cell key={`cell-${index}`}
              fill={colors[Math.floor(parseInt(entry["max_temp"]) / 10 - 3)
                % colors.length]} />))
          }
        </Scatter>
      </ScatterChart>
    );
  }
}