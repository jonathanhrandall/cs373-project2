import React, { useEffect, useState } from 'react';
import { Route } from 'react-router-dom';
import axios from 'axios';
import { Grid } from '@material-ui/core';
import Plant from '../pages/plants/Plant';
import Recipe from '../pages/recipes/Recipe';
import City from '../pages/cities/City';
import GenericCard from './GenericCard';
import LoadingSpinner from './LoadingSpinner';

type RelatedInstancesProps = {
  id: string,
  modelName: string
}

export default function RelatedInstances(props: RelatedInstancesProps) {
  const { id, modelName } = props;
  const [relatedPlants, setRelatedPlants] = useState([]);
  const [relatedRecipes, setRelatedRecipes] = useState([]);
  const [relatedCities, setRelatedCities] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    if (modelName !== 'Plants') {
      axios.get('https://homefarmer.me/api/' + modelName + '/Plants/' + id)
        .then(response => {
          setRelatedPlants(response.data.plants);
          setLoading(false);
        })
        .catch(error => {
          console.log(JSON.stringify(error));
        });
    }
    if (modelName !== 'Recipes') {
      axios.get('https://homefarmer.me/api/' + modelName + '/Recipes/' + id)
        .then(response => {
          setRelatedRecipes(response.data.recipes);
          setLoading(false);
        })
        .catch(error => {
          console.log(JSON.stringify(error));
        });
    }
    if (modelName !== 'Cities') {
      axios.get('https://homefarmer.me/api/' + modelName + '/Cities/' + id)
        .then(response => {
          setRelatedCities(response.data.cities);
          setLoading(false);
        })
        .catch(error => {
          console.log(JSON.stringify(error));
        });
    }
  }, [id, modelName]);

  if (loading) {
    return <LoadingSpinner />;
  } else if (modelName === 'Cities') {
    return (
      <div>
        <Grid container spacing={4}>
          {relatedRecipes.map((relatedRecipe: any) => (
            <Grid item xs={3} key={relatedRecipe.id}>
              <GenericCard instance={relatedRecipe} model='recipes' />
            </Grid>
          ))}
          {relatedPlants.map((relatedPlant: any) => (
            <Grid item xs={3} key={relatedPlant.id}>
              <GenericCard instance={relatedPlant} model='plants' />
            </Grid>
          ))}
        </Grid>
        <Route path={`/plants/:plantId`} children={<Plant />} />
        <Route path={`/recipes/:recipeId`} children={<Recipe />} />
      </div>
    );
  } else if (modelName === 'Plants') {
    return (
      <div>
        <Grid container spacing={4}>
          {relatedRecipes.map((relatedRecipe: any) => (
            <Grid item xs={3} key={relatedRecipe.id}>
              <GenericCard instance={relatedRecipe} model='recipes' />
            </Grid>
          ))}
          {relatedCities.map((relatedCity: any) => (
            <Grid item xs={3} key={relatedCity.id}>
              <GenericCard instance={relatedCity} model='cities' />
            </Grid>
          ))}
        </Grid>
        <Route path={`/recipes/:recipeId`} children={<Recipe />} />
        <Route path={`/cities/:cityId`} children={<City />} />
      </div>
    );
  } else {
    return (
      <div>
        <Grid container spacing={4}>
          {relatedPlants.map((relatedPlant: any) => (
            <Grid item xs={3} key={relatedPlant.id}>
              <GenericCard instance={relatedPlant} model='plants' />
            </Grid>
          ))}
          {relatedCities.map((relatedCity: any) => (
            <Grid item xs={3} key={relatedCity.id}>
              <GenericCard instance={relatedCity} model='cities' />
            </Grid>
          ))}
        </Grid>
        <Route path={`/plants/:plantId`} children={<Plant />} />
        <Route path={`/cities/:cityId`} children={<City />} />
      </div>
    );
  }
}