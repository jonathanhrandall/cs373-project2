export default function countsFromJSON(data_array: any[], field:string) {
  // Iterate through data_array and count instances with Map
  var data = new Map();
  for (var i in data_array) {
    const currInstance = data_array[i];
    const currField = currInstance[field];
    if (!data.has(currField)) {
      data.set(currField, 1);
    } else {
      data.set(currField, data.get(currField) + 1);
    }
  }
  // Convert Map to list of objects
  var dataset = [];
  for (let entry of Array.from(data.entries())) {
    let key = entry[0];
    let value = entry[1];
    const obj = { label: key, value: value };
    dataset.push(obj);
  }

  dataset.sort(function(a,b) {
    return b.value - a.value
  });
  return dataset;
}
