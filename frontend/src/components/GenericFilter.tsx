import React from 'react';
import CityFilter from '../pages/cities/CityFilter';
import RecipeFilter from '../pages/recipes/RecipeFilter';
import PlantFilter from '../pages/plants/PlantFilter';

type GenericFilterProps = {
  model: string,
  value: any,
  onChange: any
}

export default function GenericFilter(props: GenericFilterProps) {
  const { model, value, onChange } = props;
  if (model === 'plants') {
    return (<PlantFilter value={value} onChange={onChange} />);
  } else if (model === 'cities') {
    return (<CityFilter value={value} onChange={onChange} />);
  } else if (model === 'recipes') {
    return (<RecipeFilter value={value} onChange={onChange} />);
  } else {
    return (<div>Nonexistent model filter created.</div>);
  }
}