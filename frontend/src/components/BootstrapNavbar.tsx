import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Navbar, Nav } from 'react-bootstrap'
import Home from '../pages/Home';
import GenericGrid from './GenericGrid';
import About from '../pages/about/About';
import OurVisualizations from '../pages/our-visualizations/OurVisualizations'
import ProviderVisualizations from '../pages/provider-visualizations/ProviderVisualizations'
import HomeFarmerLogo from '../hflogo.svg';

const routes = [
  { name: 'Plants', route: '/plants' },
  { name: 'Cities', route: '/cities' },
  { name: 'Recipes', route: '/recipes' },
  { name: 'Our Visualizations', route: '/our-visualizations' },
  { name: 'Provider Visualizations', route: '/provider-visualizations' },
  { name: 'About', route: '/about' }
];

export default function BootstrapNavbar() {
  return (
    <div>
      <Navbar variant='dark' expand='lg' sticky='top'
        style={{ background: '#00796b' }}>
        <Navbar.Brand href='/'>
          <img src={HomeFarmerLogo} alt='HomeFarmer Logo'
            style={{ marginRight: 5 }} />HomeFarmer
        </Navbar.Brand>
        <Navbar.Toggle aria-controls='basic-navbar-nav' />
        <Navbar.Collapse id='basic-navbar-nav'>
          <Nav style={{ background: '#00796b' }}>
            {routes.map(rt => (
              <Nav.Link key={rt.name} href={rt.route}>{rt.name}</Nav.Link>
            ))}
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      <br />
      <Router>
        <Switch>
          <Route exact path='/'>
            <Home />
          </Route>
          <Route path='/about'>
            <About />
          </Route>
          <Route path='/plants'>
            <GenericGrid modelName='Plants' />
          </Route>
          <Route path='/cities'>
            <GenericGrid modelName='Cities' />
          </Route>
          <Route path='/recipes'>
            <GenericGrid modelName='Recipes' />
          </Route>
          <Route path='/our-visualizations'>
            <OurVisualizations />
          </Route>
          <Route path='/provider-visualizations'>
            <ProviderVisualizations />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}