import React from 'react';
import ReactLoading from 'react-loading';
import { Container, Typography } from '@material-ui/core';

export default function LoadingSpinner({ pageTitle }: any) {
  if (pageTitle) {
    return (
      <Container maxWidth={"xl"}>
        <Typography
          component="h1" variant="h2" align="center" color="textPrimary"
          gutterBottom>
          {pageTitle}
        </Typography>
        <div style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
          <ReactLoading
            type="spinningBubbles"
            color="#00796b"
            height={"20%"}
            width={"20%"}
          />
        </div>
      </Container>
    )
  }
  else {
    return (
      <ReactLoading
        type="spinningBubbles"
        color="#00796b"
        height={"20%"}
        width={"20%"}
      />
    );
  }
}