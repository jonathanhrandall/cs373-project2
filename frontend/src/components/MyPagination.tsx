import React from 'react'
import { MemoryRouter, Route } from 'react-router';
import { Link } from 'react-router-dom';
import Pagination from '@material-ui/lab/Pagination';
import PaginationItem from '@material-ui/lab/PaginationItem';

interface PaginationProps {
  instancesPerPage: number,
  totalInstances: number,
  paginate: any,
}

// Pagination using Material UI pagination component
export default function MyPagination(props:PaginationProps) {
  let { instancesPerPage, totalInstances, paginate } = props;
  let numPages = Math.ceil(totalInstances / instancesPerPage);
  return (
    <MemoryRouter initialEntries={['/cities']} initialIndex={0}>
      <Route>
        {({ location }) => {
          const query = new URLSearchParams(location.search);
          const page = parseInt(query.get('page') || '1', 10);
          return (
            <Pagination
              page={page}
              count={numPages}
              renderItem={(item) => (
                <PaginationItem component={Link}
                  to={`/cities${item.page === 1 ? '' : `?page=${item.page}`}`}
                  {...item} onClick={() => paginate(item.page)}
                />
              )}
            />
          );
        }}
      </Route>
    </MemoryRouter>
  )
}
