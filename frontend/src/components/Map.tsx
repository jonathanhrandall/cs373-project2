import React from 'react';
import GoogleMapReact from 'google-map-react';
import './Map.css';

type LocationKeys = {
  address?: string,
  lat: number,
  lng: number
}

type MapSectionProps = {
  location?: LocationKeys,
  zoomLevel?: number,
  mapTypeId?: string
}

function MapSection(params: MapSectionProps) {
  const { location = { lat: 30.27, lng: -97.74 },
    zoomLevel = 10, mapTypeId = "hybrid" } = params;
  return (
    <div className="map">
      <div className="google-map">
        <GoogleMapReact
          bootstrapURLKeys={{ key: process.env.REACT_APP_MAPS_API_KEY! }}
          defaultCenter={location}
          defaultZoom={zoomLevel}
          options={function () { return { mapTypeId: mapTypeId } }}
        />
      </div>
    </div>
  );
}

export default MapSection;
