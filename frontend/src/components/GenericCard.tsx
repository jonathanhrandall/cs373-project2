import React from 'react';
import CityCard from '../pages/cities/CityCard';
import PlantCard from '../pages/plants/PlantCard';
import RecipeCard from '../pages/recipes/RecipeCard';

type GenericCardProps = {
  instance: any,
  model: string,
  searchTerm?: string
}

export default function GenericCard(props: GenericCardProps) {
  const { instance, model, searchTerm } = props;
  if (model === 'plants') {
    return (<PlantCard instance={instance} searchTerm={searchTerm} />);
  } else if (model === 'cities') {
    return (<CityCard instance={instance} searchTerm={searchTerm} />);
  } else if (model === 'recipes') {
    return (<RecipeCard instance={instance} searchTerm={searchTerm} />);
  } else {
    return (<div>Nonexistent model card created.</div>);
  }
}