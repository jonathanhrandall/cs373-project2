import React from 'react';
import CitySort from '../pages/cities/CitySort';
import PlantSort from '../pages/plants/PlantSort';
import RecipeSort from '../pages/recipes/RecipeSort';

type GenericSortProps = {
  model: string,
  onChange: any
}

export default function GenericSort(props: GenericSortProps) {
  const { model, onChange } = props;
  if (model === 'plants') {
    return (<PlantSort onChange={onChange} />);
  } else if (model === 'cities') {
    return (<CitySort onChange={onChange} />);
  } else if (model === 'recipes') {
    return (<RecipeSort onChange={onChange} />);
  } else {
    return (<div>Nonexistent model sort created.</div>);
  }
}