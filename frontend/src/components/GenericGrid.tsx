import React, { useEffect, useState } from 'react';
import { Route, Switch } from 'react-router-dom';
import axios from 'axios';
import { Box, Container, Grid, TextField, Typography } from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Fuse from 'fuse.js';
import GenericCard from './GenericCard';
import GenericSort from './GenericSort';
import GenericFilter from './GenericFilter';
import Pagination from './MyPagination';
import Plant from '../pages/plants/Plant';
import City from '../pages/cities/City';
import Recipe from '../pages/recipes/Recipe';
import LoadingSpinner from './LoadingSpinner';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: { '& > *': { marginBottom: theme.spacing(1), width: '100%' } }
  })
);

const cityKeys = ['city_name', 'state_code', 'state_name']
const plantKeys = ['common_name', 'family', 'genus']
const recipeKeys = ['label', 'source']

type GenericGridProps = {
  modelName: string,
  initialQuery?: string
}

export default function GenericGrid({ modelName, initialQuery = '' }: GenericGridProps) {
  const classes = useStyles();
  const [query, setQuery] = useState(initialQuery);
  const [sortBy, setSortBy] = useState(['', '']);
  const [filterBy, setFilterBy] = useState([]);
  const [instances, setInstances] = useState([]);
  const [searchKeys, setSearchKeys] = useState([] as string[]);
  const [loading, setLoading] = useState(true);
  const [currentPage, setCurrentPage] = useState(1);
  const model = modelName.toLowerCase();

  useEffect(() => {
    if (model === 'cities') {
      axios.get('https://homefarmer.me/api/Cities')
        .then(response => {
          setInstances(response.data.cities);
          setSearchKeys(cityKeys);
          setLoading(false);
        })
        .catch(error => {
          console.log(JSON.stringify(error));
        });
    } else if (model === 'plants') {
      axios.get('https://homefarmer.me/api/Plants')
        .then(response => {
          setInstances(response.data.plants);
          setSearchKeys(plantKeys);
          setLoading(false);
        })
        .catch(error => {
          console.log(JSON.stringify(error));
        });
    } else if (model === 'recipes') {
      axios.get('https://homefarmer.me/api/Recipes')
        .then(response => {
          setInstances(response.data.recipes);
          setSearchKeys(recipeKeys);
          setLoading(false);
        })
        .catch(error => {
          console.log(JSON.stringify(error));
        });
    }
  }, [model]);

  function handleOnSearch({ currentTarget }: any) {
    setQuery(currentTarget.value);
  }

  const handleSortChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    const valueArray = (event.target.value as string).split(",");
    setSortBy(valueArray);
  };

  const handleFilterChange = (event: any, value: any) => {
    setFilterBy(value);
  }

  const fuse = new Fuse(instances, {
    keys: searchKeys,
    includeScore: true,
    threshold: .3,
    includeMatches: true,
  });

  const results = fuse.search(query);
  const instanceRes = query ? results.map(result => result.item) : instances;
  const filteredRes = filterBy.length === 0 ?
    instanceRes : filterByValues(instanceRes, filterBy, model);
  let direction = (sortBy[1] === 'descending') ? -1 : 1;
  const sortedRes = sortByProperty(filteredRes, sortBy[0], direction);
  const searchTerm = query ? query : "";
  const instancesPerPage = 12;
  const lastInstanceIdx = currentPage * instancesPerPage;
  const firstInstanceIdx = lastInstanceIdx - instancesPerPage;
  const currentInstances = sortedRes.slice(firstInstanceIdx, lastInstanceIdx);
  const paginate = (pageNum: number) => setCurrentPage(pageNum);

  if (loading) {
    return (<LoadingSpinner pageTitle={modelName} />);
  } else {
    return (
      <div>
        <Switch>
          <Route exact path={'/'}>
            <SearchResult classes={classes} modelName={modelName} model={model}
              instances={currentInstances} search={searchTerm} query={query}
              filterBy={filterBy} handleSearch={handleOnSearch}
              handleSort={handleSortChange} handleFilter={handleFilterChange} />
            <BoxWrapper ipp={instancesPerPage} length={sortedRes.length}
              paginate={paginate} />
          </Route>
          <Route exact path={'/' + model}>
            <SearchResult classes={classes} modelName={modelName} model={model}
              instances={currentInstances} search={searchTerm} query={query}
              filterBy={filterBy} handleSearch={handleOnSearch}
              handleSort={handleSortChange} handleFilter={handleFilterChange} />
            <BoxWrapper ipp={instancesPerPage} length={sortedRes.length}
              paginate={paginate} />
          </Route>
          <Route path={'/cities/:cityId'} children={<City />} />
          <Route path={'/plants/:plantId'} children={<Plant />} />
          <Route path={'/recipes/:recipeId'} children={<Recipe />} />
        </Switch>
      </div>
    );
  }
}

function SearchResult(props: any) {
  const { classes, modelName, model, instances, search, } = props;
  const { query, filterBy, handleSearch, handleSort, handleFilter } = props;
  return (
    <div>
      <Container maxWidth={"xl"}>
        <Typography component="h1" variant="h2" align="center"
          color="textPrimary" gutterBottom>{modelName}</Typography>
        <Typography />
        <Grid container spacing={2}>
          <Grid item xs={4} >
            <form className={classes.root} noValidate autoComplete="off">
              <TextField id="outlined-basic"
                label={"Search " + model}
                variant="outlined"
                value={query}
                onChange={handleSearch} />
            </form>
          </Grid>
          <Grid item xs={4} >
            <GenericSort model={model} onChange={handleSort} />
          </Grid>
          <Grid item xs={4} >
            <GenericFilter model={model} value={filterBy}
              onChange={handleFilter} />
          </Grid>
        </Grid>
        <Grid container spacing={2} alignItems="stretch" >
          {instances.map((inst: any) => (
            <Grid item xs={2} key={inst.id} >
              <GenericCard instance={inst} model={model} searchTerm={search} />
            </Grid>
          ))}
        </Grid>
      </Container>
    </div>
  );
}

function BoxWrapper(props: any) {
  const { ipp, length, paginate } = props;
  return (
    <Box display="flex" width={"100%"} height={80}>
      <Box m="auto">
        <Pagination
          instancesPerPage={ipp} totalInstances={length} paginate={paginate} />
      </Box>
    </Box>);
}

function filterByValues(objArray: any[], sources: any[], model: string) {
  let filteredArray = [];
  if (model === "cities") {
    filteredArray = objArray.filter(function (i) {
      return sources.includes(i.state_code)
    });
  } else if (model === "recipes") {
    filteredArray = objArray.filter(function (i) {
      return sources.includes(i.source)
    });
  } else {
    filteredArray = objArray.filter(function (i) {
      return sources.includes(i.family)
    });
  }
  return filteredArray;
}

function sortByProperty(objArray: any[], prop: string, direction: number) {
  if (arguments.length < 2) {
    throw new Error("ARRAY AND OBJECT PROPERTY REQUIRED ARGUMENTS");
  }
  if (!Array.isArray(objArray)) {
    throw new Error("FIRST ARGUMENT NOT AN ARRAY");
  }
  const clone = objArray.slice(0);
  const direct = arguments.length > 2 ? arguments[2] : 1; // Default ascending
  clone.sort(function (a: string, b: string) {
    if (a[prop] && b[prop]) {
      a = a[prop];
      b = b[prop];
    }
    return ((a < b) ? -1 * direct : ((a > b) ? 1 * direct : 0));
  });
  return clone;
}