import { configure, mount, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import toJson from 'enzyme-to-json';
import React from 'react';
import GoogleMapReact from 'google-map-react';
import MapSection from '../components/Map';

/* Map Unit Testing */

// Author: Grant
test('Default MapSection renders correctly', () => {
    configure({ adapter: new Adapter() });
    const wrapper = shallow(<MapSection />);
    expect(toJson(wrapper)).toMatchSnapshot();
});

// Author: Grant
test('MapSection renders correctly', () => {
    configure({ adapter: new Adapter() });
    const wrapper = shallow(<MapSection
        location={{ lat: 30.27, lng: -97.74 }}
        zoomLevel={10}
        mapTypeId="hybrid" />);
    expect(toJson(wrapper)).toMatchSnapshot();
});

// Author: Grant
test('MapSection with negative zoomLevel contains GoogleMapReact', () => {
    configure({ adapter: new Adapter() });
    const wrapper = mount(<MapSection
        location={{ lat: 30.27, lng: -97.74 }}
        zoomLevel={-5}
        mapTypeId="hybrid" />);
    expect(wrapper.contains(<GoogleMapReact />));
});

// Author: Grant
test('MapSection with invalid location contains GoogleMapReact', () => {
    configure({ adapter: new Adapter() });
    const wrapper = mount(<MapSection
        location={{ lat: 100, lng: 200 }}
        zoomLevel={10}
        mapTypeId="hybrid" />);
    expect(wrapper.contains(<GoogleMapReact />));
});

// Author: Grant
test('MapSection with invalid mapTypeId contains GoogleMapReact', () => {
    configure({ adapter: new Adapter() });
    const wrapper = mount(<MapSection
        location={{ lat: 30.27, lng: -97.74 }}
        zoomLevel={10}
        mapTypeId="livingtone" />);
    expect(wrapper.contains(<GoogleMapReact />));
});