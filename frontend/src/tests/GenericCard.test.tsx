import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import toJson from 'enzyme-to-json';
import React from 'react';
import GenericCard from '../components/GenericCard';

/* GenericCard Unit Testing */

// Author: Grant
test('GenericCard renders correctly for plants', () => {
    configure({ adapter: new Adapter() });
    const instance = {
        "common_name": "Lentil",
        "family": "Fabaceae",
        "genus": "Vicia",
        "id": "cb7a03a3-a71d-4c6c-9c6b-540c29c9ae4a",
        "image_url": "https://bs.floristic.org/image/o/d980811a7d836152332587" +
            "81ad486ce2ef99866f",
        "scientific_name": "Vicia lens",
        "vegetable": true,
        "wiki_url": "https://en.wikipedia.org/wiki/Lentil"
    }
    const wrapper = shallow(<GenericCard instance={instance} model="plants" />);
    expect(toJson(wrapper)).toMatchSnapshot();
});

// Author: Grant
test('GenericCard renders correctly for cities', () => {
    configure({ adapter: new Adapter() });
    const instance = {
        "city_name": "Anchorage",
        "humidity": 59.0,
        "id": "9ec207bb-bc3c-495a-bc1b-08a3183d952a",
        "image_url": "https://i.imgur.com/JXKee1Y.jpg",
        "latitude": 61.22,
        "longitude": -149.9,
        "max_temp": 39.0,
        "min_temp": 33.01,
        "pressure": 1020.0,
        "state_code": "AK",
        "state_name": "Alaska"
    }
    const wrapper = shallow(<GenericCard instance={instance} model="cities" />);
    expect(toJson(wrapper)).toMatchSnapshot();
});

// Author: Grant
test('GenericCard renders correctly for recipes', () => {
    configure({ adapter: new Adapter() });
    const instance = {
        "calories": 918.6531,
        "id": "b0977a4a-4f9d-4944-a36d-bb37b3b1a151",
        "image": "https://www.edamam.com/web-img/360/360d77d4296e669151b53bcd" +
            "19846ac3.jpg",
        "ingredient_lines": [
            "200 g puy lentils",
            "1 bunch of spring onions",
            "200 g ripe cherry tomatoes",
            "1 large bunch of fresh flat-leaf parsley",
            "1 large bunch of fresh mint",
            "extra virgin olive oil",
            "1 lemon"
        ],
        "label": "Lentil tabbouleh",
        "source": "Jamie Oliver",
        "total_time": 30.0,
        "url": "http://www.jamieoliver.com/recipes/vegetables-recipes/lentil-" +
            "tabbouleh/",
        "yield": 4.0
    }
    const wrapper = shallow(<GenericCard instance={instance} model="recipes" />);
    expect(toJson(wrapper)).toMatchSnapshot();
});

// Author: Grant
test('GenericCard renders correctly for nonexistent model', () => {
    configure({ adapter: new Adapter() });
    const instance = {}
    const wrapper = shallow(<GenericCard instance={instance} model="cats" />);
    expect(wrapper.contains(<div>Nonexistent model card created.</div>));
});