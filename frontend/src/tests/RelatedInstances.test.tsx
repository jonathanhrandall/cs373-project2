import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import toJson from 'enzyme-to-json';
import React from 'react';
import RelatedInstances from '../components/RelatedInstances';

/* RelatedInstances Unit Testing */

// Author: Grant
test('RelatedInstances renders correctly for plants', () => {
  configure({ adapter: new Adapter() });
  const wrapper = shallow(<RelatedInstances
    modelName="Plants"
    id="cb7a03a3-a71d-4c6c-9c6b-540c29c9ae4a" />
  );
  expect(toJson(wrapper)).toMatchSnapshot();
});

// Author: Grant
test('RelatedInstances renders correctly for recipes', () => {
  configure({ adapter: new Adapter() });
  const wrapper = shallow(<RelatedInstances
    modelName="Recipes"
    id="b0977a4a-4f9d-4944-a36d-bb37b3b1a151" />
  );
  expect(toJson(wrapper)).toMatchSnapshot();
});

// Author: Grant
test('RelatedInstances renders correctly for cities', () => {
  configure({ adapter: new Adapter() });
  const wrapper = shallow(<RelatedInstances
    modelName="Cities"
    id="9ec207bb-bc3c-495a-bc1b-08a3183d952a" />
  );
  expect(toJson(wrapper)).toMatchSnapshot();
});

// Author: Grant
test('RelatedInstances renders correctly for nonexistent model', () => {
  configure({ adapter: new Adapter() });
  const wrapper = shallow(<RelatedInstances
    modelName="Cats"
    id="nyan" />
  );
  expect(toJson(wrapper)).toMatchSnapshot();
});