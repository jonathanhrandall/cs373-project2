import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import toJson from 'enzyme-to-json';
import React from 'react';
import { Nav } from 'react-bootstrap';
import BootstrapNavbar from '../components/BootstrapNavbar';

/* BootstrapNavbar Unit Testing */

// Author: Grant
test('BootstrapNavbar contains proper links', () => {
  configure({ adapter: new Adapter() });
  const wrapper = shallow(<BootstrapNavbar />);
  expect(wrapper.containsAllMatchingElements([
    <Nav.Link href='/plants'>Plants</Nav.Link>,
    <Nav.Link href='/cities'>Cities</Nav.Link>,
    <Nav.Link href='/recipes'>Recipes</Nav.Link>,
    <Nav.Link href='/our-visualizations'>Our Visualizations</Nav.Link>,
    <Nav.Link href='/provider-visualizations'>Provider Visualizations</Nav.Link>,
    <Nav.Link href='/about'>About</Nav.Link>
  ]));
});

// Author: Grant
test('BootstrapNavbar renders correctly', () => {
  configure({ adapter: new Adapter() });
  const wrapper = shallow(<BootstrapNavbar />);
  expect(toJson(wrapper)).toMatchSnapshot();
});