import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import toJson from 'enzyme-to-json';
import React from 'react';
import MyPagination from '../components/MyPagination';

/* MyPagination Unit Testing */

// Author: Grant
test('MyPagination renders correctly', () => {
    configure({ adapter: new Adapter() });
    const totalInstances = 0;
    const instancesPerPage = 10;
    const paginate = (pageNum: number) => { pageNum }
    const wrapper = shallow(<MyPagination
        instancesPerPage={instancesPerPage}
        totalInstances={totalInstances}
        paginate={paginate}
    />);
    expect(toJson(wrapper)).toMatchSnapshot();
});