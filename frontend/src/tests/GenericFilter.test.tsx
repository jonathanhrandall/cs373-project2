import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import toJson from 'enzyme-to-json';
import React from 'react';
import GenericFilter from '../components/GenericFilter';

/* GenericFilter Unit Testing */

// Author: Grant
test('GenericFilter renders correctly for plants', () => {
  configure({ adapter: new Adapter() });
  const wrapper = shallow(<GenericFilter
    value=""
    model="plants"
    onChange={() => (1)}
  />);
  expect(toJson(wrapper)).toMatchSnapshot();
});

// Author: Grant
test('GenericFilter renders correctly for cities', () => {
  configure({ adapter: new Adapter() });
  const wrapper = shallow(<GenericFilter
    value=""
    model="cities"
    onChange={() => (1)}
  />);
  expect(toJson(wrapper)).toMatchSnapshot();
});

// Author: Grant
test('GenericFilter renders correctly for recipes', () => {
  configure({ adapter: new Adapter() });
  const wrapper = shallow(<GenericFilter
    value=""
    model="recipes"
    onChange={() => (1)}
  />);
  expect(toJson(wrapper)).toMatchSnapshot();
});

// Author: Grant
test('GenericFilter renders correctly for nonexistent model', () => {
  configure({ adapter: new Adapter() });
  const wrapper = shallow(<GenericFilter
    value=""
    model="cats"
    onChange={() => (1)}
  />);
  expect(wrapper.contains(<div>Nonexistent model filter created.</div>));
});