import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import toJson from 'enzyme-to-json';
import React from 'react';
import GenericSort from '../components/GenericSort';

/* GenericSort Unit Testing */

// Author: Grant
test('GenericSort renders correctly for plants', () => {
  configure({ adapter: new Adapter() });
  const wrapper = shallow(<GenericSort
    model="plants"
    onChange={() => (1)}
  />);
  expect(toJson(wrapper)).toMatchSnapshot();
});

// Author: Grant
test('GenericSort renders correctly for cities', () => {
  configure({ adapter: new Adapter() });
  const wrapper = shallow(<GenericSort
    model="cities"
    onChange={() => (1)}
  />);
  expect(toJson(wrapper)).toMatchSnapshot();
});

// Author: Grant
test('GenericSort renders correctly for recipes', () => {
  configure({ adapter: new Adapter() });
  const wrapper = shallow(<GenericSort
    model="recipes"
    onChange={() => (1)}
  />);
  expect(toJson(wrapper)).toMatchSnapshot();
});

// Author: Grant
test('GenericSort renders correctly for nonexistent model', () => {
  configure({ adapter: new Adapter() });
  const wrapper = shallow(<GenericSort
    model="cats"
    onChange={() => (1)}
  />);
  expect(wrapper.contains(<div>Nonexistent model sort created.</div>));
});