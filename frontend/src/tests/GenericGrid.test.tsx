import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import toJson from 'enzyme-to-json';
import React from 'react';
import GenericGrid from '../components/GenericGrid';

/* GenericGrid Unit Testing */

// Author: Grant
test('GenericGrid renders correctly for cities', () => {
    configure({ adapter: new Adapter() });
    const wrapper = shallow(<GenericGrid modelName="cities" />);
    expect(toJson(wrapper)).toMatchSnapshot();
});

// Author: Grant
test('GenericGrid renders correctly for plants', () => {
    configure({ adapter: new Adapter() });
    const wrapper = shallow(<GenericGrid modelName="plants" />);
    expect(toJson(wrapper)).toMatchSnapshot();
});

// Author: Grant
test('GenericGrid renders correctly for recipes', () => {
    configure({ adapter: new Adapter() });
    const wrapper = shallow(<GenericGrid modelName="recipes" />);
    expect(toJson(wrapper)).toMatchSnapshot();
});