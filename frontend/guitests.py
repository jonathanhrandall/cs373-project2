import os
from sys import platform

if __name__ == '__main__':
    if platform == 'win32':
        PATH = './frontend/gui_tests/chromedriver_win32.exe'
    elif platform == 'linux' or platform == 'linux2':
        PATH = './frontend/gui_tests/chromedriver_linux64'
    elif platform == 'darwin':
            PATH = './frontend/gui_tests/chromedriver_mac64'
    else:
        print(platform)
        print('Unsupported OS')
        exit(-1)

    os.system('python3 ./frontend/gui_tests/gui_tests.py ' + PATH)
