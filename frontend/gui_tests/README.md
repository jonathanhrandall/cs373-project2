# HomeFarmer GUI Tests

- Make sure you have the correct version of chromedriver installed
- If you do not have Selenium installed run this command:

```
pip install selenium
```

- From the main folder run this command:

```
python frontend/guitests.py
```
