import unittest
import time
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
import sys
import os
from selenium.webdriver.common.keys import Keys

PATH = "./frontend/gui_tests/chromedriver.exe"
BASE_URL = "https://www.homefarmer.me/"
NO_SELENIUM_WINDOW = (os.environ.get("NO_SELENIUM_WINDOW", "true").lower() == "true")

class TestNavigation(unittest.TestCase):

    @classmethod
    def setUp(inst):
        options = webdriver.ChromeOptions()
        if NO_SELENIUM_WINDOW:
            options.add_argument("--headless")
            options.add_argument("--disable-gpu")
            options.add_argument("--no-sandbox")
            options.add_argument("--start-maximized")
            options.add_argument("--window-size=1920,1080")
        inst.driver = webdriver.Chrome(PATH, options=options)
        inst.driver.implicitly_wait(3)

        # Navigate to the application home page
        inst.driver.get(BASE_URL)

    @classmethod
    def tearDown(inst):
        # Close the browser window
        inst.driver.quit()

    # Author: Grant He
    def test_has_correct_title(self):
        title = self.driver.title
        self.assertTrue(title == 'HomeFarmer')

    # Author: Grant He
    def test_home_search_button(self):
        get_started_link = self.driver.find_element_by_xpath('//button[text()="Search"]')
        get_started_link.click()
        self.assertTrue('?' in self.driver.current_url)

    # Author: Grant He
    def test_can_navigate_to_plants(self):
        plants_link = self.driver.find_element_by_link_text('Plants')
        plants_link.click()
        time.sleep(1)
        self.assertTrue('plants' in self.driver.current_url)

    # Author: Grant He
    def test_can_navigate_to_cities(self):
        cities_link = self.driver.find_element_by_link_text('Cities')
        cities_link.click()
        time.sleep(1)
        self.assertTrue('cities' in self.driver.current_url)

    # Author: Grant He
    def test_can_navigate_to_recipes(self):
        recipes_link = self.driver.find_element_by_link_text('Recipes')
        recipes_link.click()
        time.sleep(1)
        self.assertTrue('recipes' in self.driver.current_url)

    # Author: Jonathan Randall
    def test_can_navigate_to_our_vis(self):
        recipes_link = self.driver.find_element_by_link_text('Our Visualizations')
        recipes_link.click()
        time.sleep(1)
        self.assertTrue('our-visualizations' in self.driver.current_url)

    # Author: Jonathan Randall
    def test_can_navigate_to_prov_vis(self):
        recipes_link = self.driver.find_element_by_link_text('Provider Visualizations')
        recipes_link.click()
        time.sleep(1)
        self.assertTrue('provider-visualizations' in self.driver.current_url)

    # Author: Jonathan Randall
    def test_can_navigate_to_about(self):
        recipes_link = self.driver.find_element_by_link_text('About')
        recipes_link.click()
        time.sleep(1)
        self.assertTrue('about' in self.driver.current_url)

    # Author: Jonathan Randall
    def test_plant_search(self):
        plants_link = self.driver.find_element_by_link_text('Plants')
        plants_link.click()
        plant_search_input = self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div/div/div[1]/form/div/div/input"
        )
        plant_search_input.click()
        plant_search_input.send_keys("Nut")
        peanut_link = self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div[1]/div/div[2]/div[1]/a"
        )
        peanut_link.click()
        self.assertTrue('75f7b51c-07dc-48e0-87b7-7d8b9c024e66' in self.driver.current_url)

    # Author: Jonathan Randall
    def test_cities_search(self):
        cities_link = self.driver.find_element_by_link_text('Cities')
        cities_link.click()
        cities_search_input = self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div/div/div[1]/form/div/div/input"
        )
        cities_search_input.click()
        cities_search_input.send_keys("Wash")
        seattle_link = self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div[1]/div/div[2]/div[1]/div/a"
        )
        seattle_link.click()
        self.assertTrue('03d9ee56-1f7f-48b4-8286-0c6875ce00f8' in self.driver.current_url)

    # Author: Grant He
    def test_recipe_search(self):
        recipes_link = self.driver.find_element_by_link_text('Recipes')
        recipes_link.click()
        recipes_search_input = self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div/div/div[1]/form/div/div/input"
        )
        recipes_search_input.click()
        recipes_search_input.send_keys("Sponach")
        creamed_spinach_link = self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div[1]/div/div[2]/div[1]/a"
        )
        creamed_spinach_link.click()
        self.assertTrue('6a077a62-86f1-4fb3-b13c-fad8eeb6126d' in self.driver.current_url)

    # Author: Jonathan Randall
    def test_plant_sort(self):
        plants_link = self.driver.find_element_by_link_text('Plants')
        plants_link.click()
        plant_sort_select = self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div[1]/div/div[1]/div[2]/div/div/div"
        )
        plant_sort_select.click()
        time.sleep(1)
        sci_name_asc = self.driver.find_element_by_xpath(
            "/html/body/div[2]/div[3]/ul/li[4]"
        )
        sci_name_asc.click()
        baby_kiwi_link = self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div[1]/div/div[2]/div[1]/a"
        )
        baby_kiwi_link.click()
        self.assertTrue('9ffbbc9e-40ea-488f-ab0c-a7719ea72c9c' in self.driver.current_url)

    # Author: Grant He
    def test_recipe_sort(self):
        recipes_link = self.driver.find_element_by_link_text('Recipes')
        recipes_link.click()
        sort_select = self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div[1]/div/div[1]/div[2]/div/div/div"
        )
        sort_select.click()
        time.sleep(1)
        my_sort = self.driver.find_element_by_xpath(
            "/html/body/div[2]/div[3]/ul/li[8]"
        )
        my_sort.click()
        instance_link = self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div[1]/div/div[2]/div[1]/a"
        )
        instance_link.click()
        self.assertTrue('228de1a1-02d7-4726-ab5f-30fb80a5f58b' in self.driver.current_url)

    # Author: Grant He
    def test_cities_sort(self):
        link = self.driver.find_element_by_link_text('Cities')
        link.click()
        sort_select = self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div[1]/div/div[1]/div[2]/div/div/div"
        )
        sort_select.click()
        time.sleep(1)
        max_temp_dsc = self.driver.find_element_by_xpath(
            "/html/body/div[2]/div[3]/ul/li[13]"
        )
        max_temp_dsc.click()
        time.sleep(1)
        instance_link = self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div[1]/div/div[2]/div[1]/div/a"
        )
        instance_link.click()
        self.assertTrue('df1e4bb9-87b0-4194-8c9c-476b6b534853' in self.driver.current_url)

    # Helper function
    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException:
            return False
        return True

if __name__ == "__main__":

    try:
        # For linux headless only.
        # OK to fail & ignore when running in development.
        from pyvirtualdisplay import Display

        display = Display(visible=0, size=(1920, 1080))
        display.start()
    except ImportError as e:
        print('pyvirtualdisplay not found, no virtual display will be created.')
    PATH = sys.argv[1]
    unittest.main(argv=['first-arg-is-ignored'])
