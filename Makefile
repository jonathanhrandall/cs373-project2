all:

backend-unit-tests:
	echo "Running Python unit tests..."
	python3 backend/tests.py

frontend-tests:
	echo "Running Jest and Selenium test suites..."
	cd frontend/ && npm test