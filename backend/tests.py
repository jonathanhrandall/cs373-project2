import unittest
import os
from helper import *


class backend_tests(unittest.TestCase):

    # check if passed in string UUID is valid

    def test_valid_uuid(self):
        x = "cb7a03a3-a71d-4c6c-9c6b-540c29c9ae4a"
        assert valid_uuid(x)

    def test_valid_uuid2(self):
        x = ""
        assert not valid_uuid(x)

    def test_valid_uuid3(self):
        x = "cb7a03a3-a71d-4c6c-9c6b-540c29c9ae4a"
        assert valid_uuid(x)
        y = x + "g"
        assert not valid_uuid(y)

    # check if query for getting all instances of model is accurate

    def test_get_all_query(self):
        query = 'SELECT * FROM public."Plants"'
        assert get_all_query("Plants") == query

    def test_get_all_query2(self):
        try:
            get_all_query("Sameer")
            assert False
        except ValueError:
            assert True

    # check if query for getting an instance of model by ID is accurate

    def test_by_id_query(self):
        try:
            get_by_id_query("Sameer", 0)
            assert False
        except ValueError:
            assert True

    def test_by_id_query2(self):
        try:
            get_by_id_query("Cities", 0)
            assert True
        except ValueError:
            assert False

    # check if query for getting related instances of model is accurate

    def test_get_related_query(self):
        try:
            get_related_query(("Cities", "Plants"), 0)
            assert True
        except ValueError:
            assert False

    def test_get_related_query2(self):
        try:
            get_related_query(("Sammer", "Samir", "Smear"), 0)
            assert False
        except ValueError:
            assert True

    def test_get_related_query3(self):
        try:
            get_related_query(("Plants", "Plants"), 0)
            assert False
        except ValueError:
            assert True

    def test_get_related_query4(self):
        try:
            get_related_query(("Plants", "Plants"), 0)
            assert False
        except ValueError:
            assert True

    def test_get_related_query5(self):
        targetQuery = 'SELECT public."Recipes".* FROM (public.' \
        '"plants_recipes" INNER JOIN public."Recipes" ON public.' \
        '"plants_recipes".recipe_id=public."Recipes".id) WHERE public.' \
        '"plants_recipes".plant_id = \'0\''
        query = get_related_query(("Plants", "Recipes"), 0)
        assert targetQuery == query

# run all unittests
if __name__ == "__main__":
    unittest.main()
