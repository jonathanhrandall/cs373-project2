import psycopg2
import os

# start initial connection to postgres database with environment variables
connection = psycopg2.connect(
    host=os.environ.get("HOST"),
    port=os.environ.get("PORT"),
    user="homefarmer",
    password=os.environ.get("PASSWORD"),
    database="postgres",
)

# keep track of global cursor so we don't have to reconnect for every query
cursor = connection.cursor()

# get a new connection to the database and return cursor for the connection
def get_connection():
    connection = psycopg2.connect(
        host=os.environ.get("HOST"),
        port=os.environ.get("PORT"),
        user="homefarmer",
        password=os.environ.get("PASSWORD"),
        database="postgres",
    )
    return connection.cursor()

# try and execute the query passed in with our global cursor we have
def execute_query(query):
    # make sure we are using the global cursor variable
    global cursor
    try:
        # try and execute query and return results
        cursor.execute(query)
        return cursor.fetchall()
    # check if connection has dropped
    except psycopg2.Error:
        # create a new connection and execute the query again
        cursor = get_connection()
        cursor.execute(query)
        # return the results using the new, refreshed connection
        return cursor.fetchall()
