from helper import *
from flask import jsonify
from connection import *

# generalized helper method for getting all instances of a model
def get_all(model_type):
    # get results from database of all instances
    instances = execute_query(get_all_query(model_type))

    # keep track of lower-case value of model name
    lower = model_type.lower()

    # initialize resulting JSON
    result = {lower: []}

    # get field names for this specific model
    cols = map_cols[model_type]

    # loop through all instances of given model
    for instance in instances:
        # keep track of current JSON for this instances
        obj = {}
        # loop through all displayed fields of instance of model shown
        for i in range(len(cols)):
            # set field value equal to instance's corresponding value in DB
            obj[cols[i]] = instance[i]
        # add instance JSON to overall result JSON
        result[lower].append(obj)

    # return result JSON
    return jsonify(result), 200

def get_by_id(model_type, id):
    # check if ID passed in is a valid UUID
    if not valid_uuid(id):
        # if not then return an error that a valid ID was not passed in
        return jsonify(error="Valid id not provided"), 404

    # get results from database of instance with the passed in ID
    instance = execute_query(get_by_id_query(model_type, id))

    # sometimes doesn't work and needs to be tried again before failing
    if len(instance) == 0:
        instance = execute_query(get_by_id_query(model_type, id))
        if len(instance) == 0:
            # NEEDS TO BE SINGULAR
            return jsonify(error="{} id not found".format(model_type)), 404

    # initialize resulting JSON
    result = {}
    # get field names for this specific model
    cols = map_cols[model_type]
    # loop through all displayed fields of instance of model shown
    for i in range(len(cols)):
        # set field value equal to instance's corresponding value in DB
        result[cols[i]] = instance[0][i]

    # return result JSON
    return jsonify(result), 200

def get_related(model_type_1, model_type_2, id):
    # check if ID passed in is a valid UUID
    if not valid_uuid(id):
        # if not then return an error that a valid ID was not passed in
        return jsonify(error="Valid id not provided"), 404

    # get results from database of related instances with the passed in ID
    instances = execute_query(get_related_query((model_type_1, model_type_2), 
    id))

    # if there are not records matching that ID, then instance is not in DB
    if len(instances) == 0:
        # NEEDS TO BE SINGULAR
        return jsonify(error="{} id not found".format(model_type_1)), 404

    # keep track of lower-case value of related model name
    lower = model_type_2.lower()

    # initialize resulting JSON
    result = {lower: []}

    # get field names for related model passed in
    cols = map_cols[model_type_2]

    # loop through all instances of given related model
    for instance in instances:
        # keep track of current JSON for this instances
        obj = {}
        # loop through all displayed fields of instance of related model shown
        for i in range(len(cols)):
            # set field value equal to instance's corresponding value in DB
            obj[cols[i]] = instance[i]
        # add instance JSON to overall result JSON
        result[lower].append(obj)

    # return result JSON
    return jsonify(result), 200
