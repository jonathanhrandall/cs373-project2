import uuid

# get names of columns in database for Plants table
plant_cols = ("id", "common_name", "genus", "family", "scientific_name",
    "image_url", "wiki_url", "vegetable")

# get names of columns in database for Recipes table
recipe_cols = ("id", "label", "yield", "ingredient_lines", "calories",
    "total_time", "source", "url", "image")

# get names of columns in database for Cities table
city_cols = ("id", "city_name", "state_name", "state_code", "latitude",
    "longitude", "min_temp", "max_temp", "humidity", "pressure", "image_url")

# create mapping between model name and columns in database tables
map_cols = {"Plants": plant_cols, "Recipes": recipe_cols, "Cities": city_cols}

# create mapping between model name plural form and singular form
singulars = {"Plants": "Plant", "Recipes": "Recipe", "Cities": "City"}

# check if ID passed is a valid version 4 UUID
def valid_uuid(id):
    try:
        # try to parse passed in ID as version 4 UUID
        val = uuid.UUID(id, version=4)
    except ValueError:
        return False

    # check to see if values are actually the same
    return str(val) == id

# generate query for a get all instances request
def get_all_query(model_type):
    # make sure this is a valid model
    if model_type not in map_cols:
        raise ValueError
    # return query to get all instances for this model
    return 'SELECT * FROM public."{}"'.format(model_type)

# generate query for a get instance by id request
def get_by_id_query(model_type, id):
    # make sure this is a valid model
    if model_type not in map_cols:
        raise ValueError
    # return query to get specific instance for this model based on ID
    return "SELECT * FROM public.\"{}\" WHERE id = '{}'".format(model_type, id)

# generate query for a get related instances request
def get_related_query(model_types, id):
    # can only get instances of a model related to another model's instance
    if len(model_types) != 2:
        raise ValueError

    # dynamically determine join table name
    table_name = ""
    # plant comes first if it is there
    if "Plants" in model_types:
        # check which of the other two is the other model
        if "Recipes" in model_types:
            table_name = "plants_recipes"
        elif "Cities" in model_types:
            table_name = "plants_cities"
        else:
            raise ValueError
    # otherwise it has to be recipes and cities
    elif "Recipes" in model_types and "Cities" in model_types:
        table_name = "recipes_cities"
    else:
        raise ValueError

    # dynamically get name of ID column of models in the join table
    id_names = []
    # add ID column name for each model
    for model_type in model_types:
        # names reflect how they are in the join table in the postgres DB
        if model_type == "Plants":
            id_names.append("plant_id")
        elif model_type == "Recipes":
            id_names.append("recipe_id")
        elif model_type == "Cities":
            id_names.append("city_id")
        else:
            raise ValueError

    # return query to get all related instances of a model for a model object
    return 'SELECT public."{related_model}".* FROM (public."{join_table}"' \
    ' INNER JOIN public."{related_model}" ON public."{join_table}".' \
    '{related_id}=public."{related_model}".id) WHERE public."{join_table}"' \
    '.{parent_id} = \'{id}\''.format(
        related_model=model_types[1],
        join_table=table_name,
        parent_id=id_names[0],
        related_id=id_names[1],
        id=id,
    )