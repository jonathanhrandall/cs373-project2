from flask import Flask, request, render_template
from flask_cors import CORS
import random
import csv
import json
from dbCalls import *

# flask app initialization
app = Flask(
    __name__,
    static_folder="../frontend/build/static",
    template_folder="../frontend/build",
)
CORS(app)

# serve frontend statically
@app.route("/", defaults={"path": ""})
@app.route("/<path:path>")
def index(path):
    return render_template("index.html")

# get all Plants by calling helper function in dbCalls specifying "Plants"
@app.route("/api/Plants")
def get_all_plants():
    return get_all("Plants")

# get details about the specified plant identified by the ID passed in
@app.route("/api/Plants/<string:id>")
def get_plant_by_id(id):
    return get_by_id("Plants", id)

# get all of the related recipes for the specified plant identified by ID
@app.route("/api/Plants/Recipes/<string:id>")
def get_recipes_for_plant(id):
    return get_related("Plants", "Recipes", id)

# get all of the related cities for the specified plant identified by ID
@app.route("/api/Plants/Cities/<string:id>")
def get_cities_for_plant(id):
    return get_related("Plants", "Cities", id)

# get all Recipes by calling helper function in dbCalls specifying "Recipes"
@app.route("/api/Recipes")
def get_all_recipes():
    return get_all("Recipes")

# get details about the specified recipe identified by the ID passed in
@app.route("/api/Recipes/<string:id>")
def get_recipe_by_id(id):
    return get_by_id("Recipes", id)

# get all of the related plants for the specified recipe identified by ID
@app.route("/api/Recipes/Plants/<string:id>")
def get_plants_for_recipe(id):
    return get_related("Recipes", "Plants", id)

# get all of the related cities for the specified recipe identified by ID
@app.route("/api/Recipes/Cities/<string:id>")
def get_cities_for_recipe(id):
    return get_related("Recipes", "Cities", id)

# get all Cities by calling helper function in dbCalls specifying "Cities"
@app.route("/api/Cities")
def get_all_cities():
    return get_all("Cities")

# get details about the specified cities identified by the ID passed in
@app.route("/api/Cities/<string:id>")
def get_city_by_id(id):
    return get_by_id("Cities", id)

# get all of the related plants for the specified city identified by ID
@app.route("/api/Cities/Plants/<string:id>")
def get_plants_for_city(id):
    return get_related("Cities", "Plants", id)

# get all of the related recipes for the specified city identified by ID
@app.route("/api/Cities/Recipes/<string:id>")
def get_recipes_for_city(id):
    return get_related("Cities", "Recipes", id)

# start up flask server
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80, threaded=True, debug=True)
