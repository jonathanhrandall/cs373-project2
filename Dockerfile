FROM nikolaik/python-nodejs

RUN git clone https://gitlab.com/jonathanhrandall/cs373-project2.git

WORKDIR /cs373-project2

RUN cd frontend && yarn install && yarn build

RUN pip3 install -r backend/requirements.txt

EXPOSE 80

CMD python3 backend/main.py
